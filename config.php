<?php
// config.php
// 
// CLANS Web Viewer, an web application for proteinclassification.
// Copyright (C) 2012 Jens Rauch
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program. If not, see http://www.gnu.org/licenses.
//---------------------------------------------------------------------

//
//This file contains config options for administrators/hosters
//

//Number of CPU cores to use (default: no number specified -> every program decides on its self)
//Possible options:
//Default setting of each program -> "default" (default -> MUSCLE uses one core and HMMER all available)
//Specifie number -> n (e.g. 1 or 2)
$cpucores = "default";

//Alignment tool the server should use !!it must be installed on the system!! (default: MUSCLE)
//Possible options:
//MUSCLE -> "muscle"
//ClustalW2 -> "clustalw"
//ClustalOmega -> "clustalo" (multicore support)
//Let user choose -> not implemented yet
//(if you want to use one not listed change the sourcecode of muscle.php accordingly or contact the developer)
$aligntool = "muscle";
?>