<?php if(empty($_GET["refresh"])==FALSE){$refresh = $_GET["refresh"];} ?>
<!doctype html>
<!-- index.php

CLANS Web Viewer, an web application for proteinclassification.
Copyright (C) 2012 Jens Rauch

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see http://www.gnu.org/licenses. -->
<html>
	<head>
		<meta charset="utf-8" />
		<title>CLANS Web Viewer (Beta)</title>
		
		<link rel="stylesheet" href="./css/main_green.css">
		<script src="./js/jquery-1.7.2.min.js" type="text/javascript" charset="utf-8"></script>
		<link type="text/css" href="css/trontastic/jquery-ui-1.8.21.custom.css" rel="stylesheet" />
		<script type="text/javascript" src="js/jquery-ui-1.8.21.custom.min.js"></script>
		<script language="javascript" type="text/javascript">
			//this function is executed as soon as the addsequnce script finishes and starts calculation of the MSAs
			function finish(){
				uploadcomplete("Executing Muscle...","","19");
				musclecount=0;
				var name = usedgrps[musclecount].replace("/","");
				filen = dir+"/"+name+".txt";
				filen = filen.replace("\n","");
				var filesplit = filen.split("/");
				var filecount = filesplit.length;
				filecheck = filesplit[filecount-3]+"/"+filesplit[filecount-2]+"/"+filesplit[filecount-1]+".aln";
				uploadcomplete("Aligning "+usedgrps[musclecount]+"...","<p>Executing Muscle, please wait. This could take a long time.</p><p>Creating an alignment for "+name+"</p>","20");
				$('#status').show();
				muscle(filen,0,0,mainfile);
				musclewarning();
				time = 1;
				setTimeout(function() {checkready();},100);
				hmmcount=0;
			}
			//start calculation of HMMs
			function starthmm(){
				uploadcomplete("Executing HMMER...","","59");
				var hname = usedgrps[hmmcount].replace("/","");
				hfilen = dir+"/"+hname+".txt";
				hfilen = hfilen.replace("\n","");
				var hfilesplit = hfilen.split("/");
				var hfilecount = hfilesplit.length;
				hfilecheck = hfilesplit[hfilecount-3]+"/"+hfilesplit[hfilecount-2]+"/"+hfilesplit[hfilecount-1]+".hmm";
				if(hmmwrite[hmmcount+1]>=2){
					uploadcomplete("Creating HMM for "+usedgrps[hmmcount]+"...","<p>Executing HMMER, please wait. This could take a while.</p><p>Creating a HMM for "+hname+"</p>","60");
					if(typeof(ft)=='undefined'){ hmmer(hfilen,0,0,dir,0); }
					else{ hmmer(hfilen,0,0,dir,ft); }
					setTimeout(function() {checkreadyhmm(0);},200);
				}
				else{
					uploadcomplete("Creating HMM for "+usedgrps[hmmcount]+"...","<p>HMM for "+usedgrps[hmmcount]+" could not be created, group contains only one sequence!</p>",far);
					hmmcount++;
					if(hmmcount>usedgrps.length){ save(); }
					else{ starthmm(); }
				}
				time = 1;
			}
			//check if muscle is running
			function checkmuscle(file,check,pid,fpath){
				running = $.ajax({
					type: 'POST',
					async: false,
					url: './back/muscle.php',
					data: ({
						a: file,
						b: check,
						c: pid,
						d: fpath
					})
				}).responseText;
			}
			//check if hmmer is running
			function checkhmmer(file,check,pid,dir,ft){
				running = $.ajax({
					type: 'POST',
					async: false,
					url: './back/hmmer.php',
					data: ({
						a: file,
						b: check,
						c: pid,
						d: dir,
						e: ft
					})
				}).responseText;
			}
			//set time interval for checkmuscle and execute code if its finished
			function checkready(){
				checkmuscle(0,4,opid,0);
				if(time==1){interv=1000;}
				else if(time==2){interv=2000;}
				else if(time==3){interv=3000;}
				else if(time==5){interv=5000;}
				else if(time==20){interv=10000;}
				if(running==true){
					exe = setTimeout(function() {checkready();}, interv);
					time++;
				}
				else{
					muscle(filen,2,0,mainfile);
					musclecount++;
					if(musclecount<usedgrps.length){
						var far =  Math.round(20 +(((( musclecount / usedgrps.length ) * 100 ) / 3)-1));
						var name = usedgrps[musclecount].replace("/","");
						filen = dir+"/"+name+".txt";
						filen = filen.replace("\n","");
						var filesplit = filen.split("/");
						var filecount = filesplit.length;
						filecheck = filesplit[filecount-3]+"/"+filesplit[filecount-2]+"/"+filesplit[filecount-1]+".aln";
						uploadcomplete("Aligning "+usedgrps[musclecount]+"...","<p>Creating an alignment for "+name+"</p>",far);
						muscle(filen,0,0,mainfile);
						console.log(opid);
						$(opid).appendTo('div.log');
						$("div.log").scrollTop($("div.log").get(0).scrollHeight);
						musclewarning();
						time=1;
						exe = setTimeout(function() {checkready();},100);
					}
					else if(musclecount==usedgrps.length){
						uploadcomplete("Group Alignments finished, creating overall Alingment","<p>Group Alignments finished, creating overall Alingment.</p>","55");
						//create alignment from all grps
						filen = dir+"/background.txt";
						filen = filen.replace("\n","");
						muscle(filen,0,0,mainfile);
						musclewarning();
						time=1;
						exe = setTimeout(function() {checkready();},100);
					}
					else{
						uploadcomplete("Alignment finished, creating HMMs","<p>Alignment finished, creating HMMs.</p>","58");
						starthmm();
					}
				}
			}
			//set time interval for checkhmmer and execute code if its finished
			function checkreadyhmm(write){
				if(write!=1){
					checkhmmer(0,4,opid,0);
					if(time==1){interv=500;}
					else if(time==2){interv=1000;}
					else if(time==3){interv=1500;}
					else if(time==5){interv=2500;}
					else if(time==20){interv=5000;}
					if(running==true){
						exe = setTimeout(function() {checkreadyhmm(0);}, interv);
						time++;
					}
					else{
						hmmer(hfilen,2,0,dir);
						hmmcount++;
						if(hmmcount<usedgrps.length){
							var far =  Math.round(60 +(((( hmmcount / usedgrps.length ) * 100 ) / 3)-1));
							var hname = usedgrps[hmmcount].replace("/","");
							hfilen = dir+"/"+hname+".txt";
							hfilen = hfilen.replace("\n","");
							var hfilesplit = hfilen.split("/");
							var hfilecount = hfilesplit.length;
							hfilecheck = hfilesplit[hfilecount-3]+"/"+hfilesplit[hfilecount-2]+"/"+hfilesplit[hfilecount-1]+".hmm";
							if(hmmwrite[hmmcount+1]>=2){
								uploadcomplete("Creating a HMM for "+usedgrps[hmmcount]+"...","<p>Creating a HMM for "+hname+"</p>",far);
								if(typeof(ft)=='undefined'){ hmmer(hfilen,0,0,dir,0); }
								else{ hmmer(hfilen,0,0,dir,ft); }
								$(opid).appendTo('div.log');
								$("div.log").scrollTop($("div.log").get(0).scrollHeight);
								exe = setTimeout(function() {checkreadyhmm(0);},200);
							}
							else{
								uploadcomplete("Creating a HMM for "+usedgrps[hmmcount]+"...","<p>HMM for "+usedgrps[hmmcount]+" could not be created, group contains only one sequence!</p>",far);
								exe = setTimeout(function() {checkreadyhmm(1);},200);
							}
							time=1;
						}
						else if(hmmcount==usedgrps.length){
							uploadcomplete("HMMs for every group finished, creating overall HMM","<p>HMMs for every group finished, creating overall HMM.</p>","99");
							//create alignment from all grps
							hfilen = dir+"/background.txt";
							hfilen = hfilen.replace("\n","");
							if(typeof(ft)=='undefined'){ hmmer(hfilen,0,0,dir,0); }
							else{ hmmer(hfilen,0,0,dir,ft); }
							time=1;
							exe = setTimeout(function() {checkreadyhmm(1);},200);
						}
						else{
							uploadcomplete("HMMs created successfully","<p>HMMs created & compressed successfully</p>","100");
							save();
						}
					}
				}
				else if(write==1){
					hmmcount++;
					if(hmmcount<usedgrps.length){
						var far =  Math.round(60 +(((( hmmcount / usedgrps.length ) * 100 ) / 3)-1));
						var hname = usedgrps[hmmcount].replace("/","");
						hfilen = dir+"/"+hname+".txt";
						hfilen = hfilen.replace("\n","");
						var hfilesplit = hfilen.split("/");
						var hfilecount = hfilesplit.length;
						hfilecheck = hfilesplit[hfilecount-3]+"/"+hfilesplit[hfilecount-2]+"/"+hfilesplit[hfilecount-1]+".hmm";
						if(hmmwrite[hmmcount+1]>=2){
							uploadcomplete("Creating HMM for "+usedgrps[hmmcount]+"...","<p>Executing HMMER for "+hname+"</p>",far);
							if(typeof(ft)=='undefined'){ hmmer(hfilen,0,0,dir,0); }
							else{ hmmer(hfilen,0,0,dir,ft); }
							$(opid).appendTo('div.log');
							$("div.log").scrollTop($("div.log").get(0).scrollHeight);
							exe = setTimeout(function() {checkreadyhmm(0);},200);
						}
						else{
							uploadcomplete("Creating HMM for "+usedgrps[hmmcount]+"...","<p>HMM for "+usedgrps[hmmcount]+" could not be created, group contains only one sequence!</p>",far);
							exe = setTimeout(function() {checkreadyhmm(1);},200);
						}
						time=1;
					}
					else if(hmmcount==usedgrps.length){
						uploadcomplete("HMMs for every group finished, creating overall HMM","<p>HMMs for every group finished, creating overall HMM.</p>","99");
						//create alignment from all grps
						hfilen = dir+"/background.txt";
						hfilen = hfilen.replace("\n","");
						if(typeof(ft)=='undefined'){ hmmer(hfilen,0,0,dir,0); }
						else{ hmmer(hfilen,0,0,dir,ft); }
						time=1;
						exe = setTimeout(function() {checkreadyhmm(1);},200);
					}
					else{
						uploadcomplete("HMMs created successfully","<p>HMMs created & compressed successfully</p>","100");
						save();
					}
				}
			}
			//call muscle
			function muscle(file,check,pid,fpath){
				if(typeof(opid)=='undefined'){opid=0;}
				pid = $.ajax({
					type: 'POST',
					async: false,
					url: './back/muscle.php',
					data: ({
						a: file,
						b: check,
						c: opid,
						d: fpath
					})
				}).responseText;
				opid=pid;
			}
			function musclewarning(){
				var mwarning = $.ajax({
					type: 'POST',
					async: false,
					url: './back/muscle.php',
					data: ({
						b: "5",
						d: dir
					})
				}).responseText;
				if(mwarning != ""){	writetolog(mwarning); }
			}
			//call hmmer
			function hmmer(file,check,pid,dir,ft){
				pid = $.ajax({
					type: 'POST',
					async: false,
					url: './back/hmmer.php',
					data: ({
						a: file,
						b: check,
						c: opid,
						d: dir,
						e: ft
					})
				}).responseText;
				opid=pid;
			}
			function deact(){
				$('input.fileb').attr('disabled', 'disabled');
				$('input.ftpup').attr('disabled', 'disabled');
			}
			//update progress bar and log
			function uploadcomplete(action,output,number){
				$(action).appendTo('div.progressbar');
				$(output).appendTo('div.log');
				newText = action;
				newVal = parseInt(number);
				$('#progressbar').progressbar('value', newVal);
				$("div.log").scrollTop($("div.log").get(0).scrollHeight);
			}
			function writetolog(text){
				$(text).appendTo('div.log');
				$("div.log").scrollTop($("div.log").get(0).scrollHeight);
			}
			//execute if everything is finished
			function save(){
				hmmer(hfilen,3,0,dir);
				$('#status').hide();
				$('#stopit').hide();
				add = '<p><b>Done!<br></b><br>\n'+
				'please press this button to continue:\n'+
				'<form action="./front/main.php" method="get">\n'+
				'<button name="id" value="'+saveid+'" class="continue">Continue</button>\n'+
				'</form></p>'
				$(add).appendTo('#saveit');
				$('#saveit').show();
			}
		</script>   
		</head>
<body>
	<?php include("./config.php"); ?>
	<div id="mainb">
		<div id="header">
			<span class="htext"><a href="./index.php" class="htext">CLANS Web Viewer</a></span><span class="htextadd">Beta</span>
			<span id="ihome"><a class="mentryactive" id="home">Home</a></span>
			<span id="iupload"><a class="mentry" id="mupload">Upload Files</a></span>
			<span id="ihelp"><a class="mentry" id="mhelp">Help</a></span>
		</div>
		<div class="tooltip"></div>
		<div class="delfile" title="Confirm"><p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Do you really want to delete this file?</p></div>
		<?php include("./front/about.html"); ?>
		<div id="grpview">
			<div id="showsample" class="stext">
				<div id="headw"><a href="./clans_web_viewer-help.pdf#page=3" target="_blank"><div id="sampleinfo" class="infopic"></div></a><div class="headtext">Sample:</div></div>
				<div id="inner">
					Hox Protein example:<br />
					<form action="./front/main.php" method="get">
						<button class="idb" name="id" value="sample">View example file</button>
					</form>
				</div>
			</div>
			<br />
			<div id="showsample" class="stext">
				<div id="headw"><a href="./clans_web_viewer-help.pdf#page=3" target="_blank"><div id="fileinfo" class="infopic"></div></a><div class="headtext">Available Files:</div></div>
				<div id="inner">
				Browse the list of available CLANS files:<br />
				<form action="./front/main.php" method="post">
				<?php
					$thisdir = getcwd()."/saved_files/";
					$handle = opendir("$thisdir");
					$ic=0;
					while (false !== ($file = readdir($handle))) {
						if(is_dir($thisdir."/".$file) AND $file!="." AND $file!=".." AND $file!="sample"){
							$ic++;
						}
					}
					closedir($handle);
					$in=0;
					$handle = opendir("./saved_files");
					if($ic>=20){ $ic=20; }
					echo "<select name='id' id='idchoose' class='chooseclans' size='".$ic."'>";
					while (false !== ($file = readdir($handle))) {
						if(is_dir($thisdir."/".$file) AND $file!="." AND $file!=".." AND $file!="sample"){
							$showfname = explode(".",$file);
							$namefile = "";
							for($i=1;$i<count($showfname);$i++){
								$namefile .= $showfname[$i];
							}
							if($in % 2 == 0){
								if($showfname[0]==0){
									echo "<option class='optndark' value='".$file."'>".$namefile."</option>";
								}
								else{
									echo "<option class='optndark' value='".$file."'>#".$showfname[0]."&nbsp;".$namefile."</option>";
								}
							}
							else{
								if($showfname[0]==0){
									echo "<option class='optdark' value='".$file."'>".$namefile."</option>";
								}
								else{
									echo "<option class='optdark' value='".$file."'>#".$showfname[0]."&nbsp;".$namefile."</option>";
								}
							}
							$in++;
						}
					}
					echo "</select><br />";
					closedir($handle);
				?>
				<!-- Optional: Don't draw the groups<input type="checkbox" id="groups" name="groups"><br />
				Optional: Include specified size of points<input type="checkbox" id="size" name="size"><br /> -->
				<input type="submit" class="ftpchooseb" value="Choose for 3D view" disabled/><button type="button" class="delete" disabled/>Delete this file</button><br />
				If you have chosen a CLANS file above you can directly compare sequences to it by pasting them here:<br /> 
				<textarea name="addfasta" id="addfasta" rows="6" cols="40"></textarea><br />
				<input type="submit" class="ftpchoosebseq" value="Choose and add sequences" disabled/><br />
				</form>
				<br />
				</div>
			</div>
			<div id="dialog-modal" title="Please wait!">
				<p>Your file is opening now.</p>
				<p>This could take up to a few minutes depending on the filesize and your computers processing power, please be patient.</p>
			</div>
		</div>
		<div id="grpup">
			<div id="ftpup">
				<div id="headw"><a href="./clans_web_viewer-help.pdf#page=13" target="_blank"><div id="ftpupinfo" class="infopic"></div></a><div class="headtext">FTP Upload:</div></div>
				<div id="inner">
				<p class="stext">
				Upload files via FTP<br />
				<div class="stext" id="ftpbuttons">
					<button id="ftpinfo">Show me how to do it</button><br>
					<button id="ftpgo">I already know what to do</button>
				</div>
				</p>
				<div id="showftpinfo" class="ltext"><br />
					Just follow this guide to upload the files:<br />
					(If you need more help please consult the help-file by clicking <a href="help" target="_blank">here</a>)
					<ul>
						<li>Open your favorite FTP/SCP programm. (If you do not have one we recommend <a href="http://filezilla-project.org/" target="_blank">FileZilla</a>)</li>
						<li>Add a new connection using the IP-Address and Login of your server (Ask your serveradmin)</li>
						<li>Navigate to the webfolder (via SCP most likely /var/www/localhost/htdocs/) find the hox3dviewer folder and select the subfolder ftpuploads</li>
						<li>Put your CLANS file (and optional *.hmm files) in this folder</li>
						<li>Press the "I'am ready"-Button, choose your file, press the "Choose" button and wait for the programm to finish</li>
					</ul>
					<button id="ftpgo">I'm ready</button>
				</div>
				<br /><br />
			</div>
			</div>
			<div id="upload" class="stext">
				<div id="headw"><a href="./clans_web_viewer-help.pdf#page=13" target="_blank"><div id="directupinfo" class="infopic"></div></a><div id="ctext" class="headtext">Direct Upload:</div></div>
				<div id="inner">
				<p class="noftptext"><b>Choose a CLANS file to upload</b></p>
				<form class="phpupform" action="./back/parse.php?uform=direct" method="post" target="upload_target" enctype="multipart/form-data">
				<p id="uform">
					<input type="file" name="file" id="file" class="file" />
					<br />
					<input type="submit" name="submit" value="Submit" class="fileb" disabled="disabled"/><br />
				</p>
				<iframe id="upload_target" name="upload_target" style="width:0;height:0;border:0px solid #fff;"></iframe>
				</form>
				<form action="./index.php?refresh=true" method="get">
					<button class="refreshb" name="refresh" value="true">refresh</button>
				</form>
				<form class="ftpupform" action="./back/parse.php?uform=ftp" method="post" target="upload_target_ftp" enctype="multipart/form-data">
					<p class="ftpform">
							<?php
								$handle = opendir("./ftpuploads");
								$ic=0;
								while (false !== ($file = readdir($handle))) {
									if($file!="." && $file!=".."){
										$ic++;
									}
								}
								closedir($handle);
								$in=0;
								$handle = opendir("./ftpuploads");
								if($ic>=20){ $ic=20; }
								echo "<select name='passover' class='chooseclans' size='".$ic."'>";
								while (false !== ($file = readdir($handle))) {
									if($in % 2 == 0){
										if($file!="." && $file!=".."){
											echo "<option class='optndark' value='".$file."'>".$file."</option>";
											$in++;
										}
									}
									else{
										if($file!="." && $file!=".."){
											echo "<option class='optdark' value='".$file."'>".$file."</option>";
											$in++;
										}
									}
								}
								echo "</select><br />";
								closedir($handle);
							?>
							<br><p class="addoptclick" style="cursor: pointer"><b>Additional options [click to toggle]</b></p>
							<div class="addopt">
								Fragment threshold: <input class="number" value=0 step="0.1" type="number" min="0" max="1"> (HMMER default = 0.5, default in this web-application = 0)
							</div>
						<input type="submit" class="ftpupb" value="Choose"/><br />
					</p>
					<iframe id="upload_target_ftp" name="upload_target_ftp" style="width:0;height:0;border:0px solid #fff;"></iframe>
				</form>
				</div>
				<div id="progressbar">
					<span class="pblabel">Uploading...</span>
				</div>
				<div id="stopit">
					<button>Stop!</button><br>
					<p id="back"><b>You can get back to the index <a href="./index.php">here</a></b></p>
				</div>
				<div id="status">
					<p style="color: red">This can take a very long time! <br /><b>Please DO NOT press the refresh/F5 button in your browser or close it.</b><br /> If you want to know if it is still working please click here:<br></p>
					<?php if($aligntool=="muscle"){ ?> <button class="status">Check Progress of Muscle</button> <?php }
					else{ ?> <button class="status" disabled>Check Progress of Muscle</button> Note: You are not using MUSCLE! A progress check for clustal has not been implemented yet, please be patient and wait for new releases of this tool if you need this feature.<?php } ?>
				</div>
				<div class="log"></div>
				<div id="saveit">
				</div>
				<br />
			</div>
		</div>
		<div id="footer">
			<span class="fooright">About</span>
		</div>
	</div>
	<script>
		(function(){
			//initialize Tooltip
			var tooltip = $('<div class="tooltip"></div>').text('');
			$(tooltip).appendTo('body');
			//hide & show panels
			calc = false;
			<?php if(empty($refresh)){ ?>
				$('.refreshb').hide();
				$('.addopt').hide();
				$('#grpup').hide();
				newText = "Uploading...";
				$('#progressbar').hide();
				$('#stopit').hide();
				$('div#showftpinfo').hide();
				$('#status').hide();
				$('#back').hide();
				$('div.log').hide();
				$('#notified').hide();
				$('.ftpupform').hide();
			<?php }	else{ ?>
				$('#grpup').hide();
				$('.addopt').hide();
				newText = "Uploading...";
				$('#progressbar').hide();
				$('#stopit').hide();
				$('div#showftpinfo').hide();
				$('#status').hide();
				$('#back').hide();
				$('div.log').hide();
				$('#notified').hide();
				$('.ftpupform').hide();
				$('#grpup').show();
				$('#grpview').hide();
				$('#noftptext').show();
				$('span#ihome').children('a').attr('class', 'mentry');
				$('span#iupload').children('a').attr('class', 'mentryactive');
				$('span#ihelp').children('a').attr('class', 'mentry');
				$('.noftptext').hide();
				$('#ctext').text("Choose uploaded file:");
				$('#directupinfo').mouseenter(function(){
					tooltip.html('Choose the file you uploaded via FTP/SCP from the list below.<br>See manual 1.1.3')
					.css({
						top: $(this).position().top+30,
						left: $(this).position().left+10
					})
					.fadeIn('slow'); })
					.mouseleave(function(){
						tooltip.fadeOut('slow');
				});
				$('.ftpupform').show();
				$('.phpupform').hide();
			<?php } ?>
			$('.addoptclick').on('click', function(){
				if($('.addopt').css('display')=="none"){ $('.addopt').show(); }
				else{ $('.addopt').hide(); }				
			});
			//create about popup
			$( "#dialog" ).dialog({ autoOpen: false, width: 700, position: "top" });
			$( ".delfile" ).dialog({ 
				autoOpen: false,
				modal: true,
				resizable: false,
				buttons: {
					Delete: function() {
						did = $("select option:selected").attr('value');
						var delresult = $.ajax({
							type: 'POST',
							async: false,
							url: './back/delete.php',
							data: ({
								a: did
							})
						}).responseText;
						$(".delfile").html(delresult);
						$(".delfile").dialog( "option", "buttons", { "Ok": function() { document.location.reload(true); }} );
					},
					Cancel: function() {
						$( this ).dialog( "close" );
					}
				}
			});
			//show about when clicking on the link
			$( "span.fooright").on('click', function(){
				$("#dialog").dialog('open');
			});
			$("#progressbar").progressbar({
				value: 0,
				change: function(event, ui) {
					$('.pblabel', this).text(newText);
				}
			});
			$('input.number').on('change', function(){
				ft = $('input.number').val();
			});
			$('input.fileb').on('click', function(){
				$('#grpview').hide();
				$('#uform').hide();
				$('#ftpup').hide();
				$('.noftptext').hide();
				$('#stopit').show();
				$('#progressbar').show();
				$('div.log').show();
				$('div.log').css("height","500");
				$('div#upload').css("width", "90%");
				calc = true;
				$('#ctext').text("Working... please wait");
				$('#directupinfo').mouseenter(function(){
					tooltip.html('Your file is beeing processed, please be patient. You can view the output in the log below.')
					.css({
						top: $(this).position().top+30,
						left: $(this).position().left+10
					})
					.fadeIn('slow'); })
					.mouseleave(function(){
						tooltip.fadeOut('slow');
				});
			});
			$('input.ftpupb').on('click', function(){
				$('.refreshb').hide();
				$('#grpview').hide();
				$('.ftpupform').hide();
				$('.noftptext').hide();
				$('#ftpup').hide();
				$('#stopit').show();
				$('#progressbar').show();
				$('div.log').show();
				$('div.log').css("height","500");
				$('div#upload').css("width", "90%");
				calc = true;
			});
			$('button#ftpinfo').on('click', function(){
				$('div#showftpinfo').show();
				$('#ftpgo').hide();
				$('.phpupform').hide();
				$('.noftptext').hide();
				$('#ftpbuttons').hide();
				$('#ctext').text("Choose uploaded file:");
				$('#directupinfo').mouseenter(function(){
					tooltip.html('After you have pressed the button "I am ready" you can choose the file you uploaded via FTP/SCP from the list below<br>See manual 2.2')
					.css({
						top: $(this).position().top+30,
						left: $(this).position().left+10
					})
					.fadeIn('slow'); })
					.mouseleave(function(){
						tooltip.fadeOut('slow');
				});
			});
			$('button#ftpgo').on('click', function(){
				$('.noftptext').hide();
				$('#ctext').text("Choose uploaded file:");
				$('#directupinfo').mouseenter(function(){
					tooltip.html('Choose the file you uploaded via FTP/SCP from the list below.<br>See manual 1.1.3')
					.css({
						top: $(this).position().top+30,
						left: $(this).position().left+10
					})
					.fadeIn('slow'); })
					.mouseleave(function(){
						tooltip.fadeOut('slow');
				});
				$('.ftpupform').show();
				$('.phpupform').hide();
				$('.refreshb').show();
			});
			$( "#dialog-modal" ).dialog({
				height: 200,
				width: 500,
				modal: true,
				autoOpen: false
			});
			$('.ftpchooseb').on('click', function(){
				$( "#dialog-modal" ).dialog( "open" );
			});
			$('.ftpchoosebseq').on('click', function(){
				$( "#dialog-modal" ).dialog( "open" );
			});
			$('.idb').on('click', function(){
				$( "#dialog-modal" ).dialog( "open" );
			});
			$('a#home').on('click', function(){
				if(calc!=true){
					$('#grpview').show();
					$('#grpup').hide();
					$('span#ihome').children('a').attr('class', 'mentryactive');
					$('span#iupload').children('a').attr('class', 'mentry');
					$('span#ihelp').children('a').attr('class', 'mentry');
				}
				else{ alert("A calculation is running, please be patient or press the \"Stop!\" button"); }
			});
			$('a#mupload').on('click', function(){
				if(calc!=true){
					$('#grpup').show();
					$('#grpview').hide();
					$('#noftptext').show();
					$('span#ihome').children('a').attr('class', 'mentry');
					$('span#iupload').children('a').attr('class', 'mentryactive');
					$('span#ihelp').children('a').attr('class', 'mentry');
				}
				else{ alert("A calculation is running, please be patient or press the \"Stop!\" button"); }
			});
			$('a#mhelp').attr({target: '_blank', href  : './clans_web_viewer-help.pdf'});
			$('#sampleinfo').mouseenter(function(){
				tooltip.html('You can view the Hox Protein CLANS file here if no other file was uploaded.<br>See manual 1.1.2')
				.css({
					top: $(this).position().top+30,
					left: $(this).position().left+10
				})
				.fadeIn('slow'); })
				.mouseleave(function(){
					tooltip.fadeOut('slow');
			});
			$('#fileinfo').mouseenter(function(){
				tooltip.html('The following list shows all available CLANS files. You can choose one and optionally past your own sequences to compare them to the file.<br>See manual 1.1.3')
				.css({
					top: $(this).position().top+30,
					left: $(this).position().left+10
				})
				.fadeIn('slow'); })
				.mouseleave(function(){
					tooltip.fadeOut('slow');
			});
			$('#ftpupinfo').mouseenter(function(){
				tooltip.html('This is the recommended upload feature! If you don\'t know how to use it follow the instructions under "Show me how to do it".<br>See manual 2.2')
				.css({
					top: $(this).position().top+30,
					left: $(this).position().left+10
				})
				.fadeIn('slow'); })
				.mouseleave(function(){
					tooltip.fadeOut('slow');
			});
			$('#directupinfo').mouseenter(function(){
				tooltip.html('If you have a very small file you want to see, you can also use this upload method. (Not recommended). Note: You cannot add your own .hmm files with this upload method!<br>See manual 2.3')
				.css({
					top: $(this).position().top+30,
					left: $(this).position().left+10
				})
				.fadeIn('slow'); })
				.mouseleave(function(){
					tooltip.fadeOut('slow');
			});
			$('#helpinfo').mouseenter(function(){
				tooltip.html('You can view the help file below.')
				.css({
					top: $(this).position().top+30,
					left: $(this).position().left+10
				})
				.fadeIn('slow'); })
				.mouseleave(function(){
					tooltip.fadeOut('slow');
			});
			$('#stopit').children('button').on('click', function(){
				exe = clearTimeout(exe);
				muscle(0,1,opid,0);
				hmmer(0,1,opid,0);
				$('#back').show();
				$('#status').hide();
				$('#stopit').children('button').attr('disabled', 'disabled');
				uploadcomplete("Stopped","<p>All calculations have been stopped...</p>","100");
				calc=false;
			});
			$('button.status').on('click', function(){
				//check progress
				progress = $.ajax({
					type: 'POST',
					async: false,
					url: './back/muscle.php',
					data: ({
						a: filen,
						b: 3,
						c: 0,
						d: mainfile
					})
				}).responseText;
				$(progress).appendTo('div.log');
				$("div.log").scrollTop($("div.log").get(0).scrollHeight);
			});
			$input = $('input.id');
			$input.keyup(function(){
				var val = $input.val();
				if(val.length>0){ $('input.idb').removeAttr('disabled'); }
				else{ $('input.idb').attr('disabled', 'disabled'); }
			});
			if($("select#idchoose").val() != null){
				$('.ftpchooseb').removeAttr('disabled');
			}
			$('select#idchoose').change(function(){
				$('.ftpchooseb').removeAttr('disabled');
				$('.delete').removeAttr('disabled');
				if($('#addfasta').val().length>0){
					$('.ftpchoosebseq').removeAttr('disabled');
					$('.ftpchooseb').attr('disabled','disabled');
				}
			});
			$('button.delete').on('click', function(){
				$('button.delete').removeClass("delete").addClass("confdelete");
				$('button.confdelete').on('click', function(){
					$(".delfile").dialog('open');
				});
			});
			//activate "add" button if a sequence is pasted (keyboard)
			$('#addfasta').keyup(function(){
				if($('.ftpchooseb').attr('disabled')!="disabled"){
					if($('#addfasta').val().length>0){
						$('.ftpchoosebseq').removeAttr('disabled');
						$('.ftpchooseb').attr('disabled','disabled');
					}
					else{
						$('.ftpchoosebseq').attr('disabled', 'disabled');
						$('.ftpchooseb').removeAttr('disabled');
					}
				}
			});
			//activate "add" button if a sequence is pasted
            $("#addfasta").bind('paste', function(e) {
            	if($('.ftpchooseb').attr('disabled')!="disabled"){
					$('.ftpchoosebseq').removeAttr('disabled');
					$('.ftpchooseb').attr('disabled','disabled');
				}
			});
			$input2 = $('input.file');
			$input2.change(function(){
				var val2 = $input2.val();
				if(val2.length>0){ $('input.fileb').removeAttr('disabled'); }
				else{ $('input.fileb').attr('disabled', 'disabled'); }
			});
		})();
	</script>
</body>
</html>