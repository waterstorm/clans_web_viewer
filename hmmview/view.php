<!doctype html>
<!-- view.php

CLANS Web Viewer, an web application for proteinclassification.
Copyright (C) 2012 Jens Rauch

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see http://www.gnu.org/licenses. -->
<html>
	<head>
		<meta charset="utf-8" />
		<title>HMM Viewer (Beta)</title>
		<link rel="stylesheet" href="../css/view.css">
		<script src="../js/three.min-r50.js" type="text/javascript" charset="utf-8"></script>
		<script src="../js/Stats.js" type="text/javascript" charset="utf-8"></script>
		<script src="../js/CamControls.js" type="text/javascript" charset="utf-8"></script>
		<script src="../js/jquery-1.7.2.min.js" type="text/javascript" charset="utf-8"></script>
		<link type="text/css" href="../css/trontastic/jquery-ui-1.8.21.custom.css" rel="stylesheet" />
		<script type="text/javascript" src="../js/jquery-ui-1.8.21.custom.min.js"></script>
	</head>
	<script>
		//initialize some needed variables for three.js
		var container, renderer, camera, scene, color, controls, projector;
		var mouse = new THREE.Vector2();
		var objects = [];
		var WIDTH, HEIGHT;
		var firstround = true;
		//define one letter code & color for AS
		var as = ["A","C","D","E","F","G","H","I","K","L","M","N","P","Q","R","S","T","V","W","Y"];
		var ascol1 = ["0x6699FF","0x6699CC","0xFF33CC","0xFF66FF","0x6666FF","0xFF9900","0x66FFFF","0x6666CC","0xFF0033","0x0033FF","0x6600FF","0x66FF00","0xFFCC66","0x00FF66","0xCC0000","0x66CC00","0x33CC66","0x0000CC","0x000099","0x33FFCC"];
		var ascol = [0x6699FF,0x6699CC,0xFF33CC,0xFF66FF,0x6666FF,0xFF9900,0x66FFFF,0x6666CC,0xFF0033,0x0033FF,0x6600FF,0x66FF00,0xFFCC66,0x00FF66,0xCC0000,0x66CC00,0x33CC66,0x0000CC,0x000099,0x33FFCC];
		function init(){
			// set basic config - container, camera, renderer, controls
			container = document.getElementById('container');
			WIDTH = document.getElementById("container").offsetWidth,
			HEIGHT = 900; 
			camera = new THREE.PerspectiveCamera( 45, WIDTH / HEIGHT, 1, 10000);
			scene = new THREE.Scene();
			scene.add( camera );
			renderer = new THREE.WebGLRenderer( { antialias: true } );
			renderer.sortObjects = false;
			renderer.setSize(WIDTH, HEIGHT);
			projector = new THREE.Projector();
			container = document.getElementById('container');
			container.appendChild(renderer.domElement);
			camera.position.z = 60;
			camera.position.x = 50;
			camera.position.y = 5;
			controls = new THREE.CamControls( camera, container );
			targ = new THREE.Vector3( 50, 0, 0);
			controls.noZoom = true;
			controls.target = targ;
			stats = new Stats();
			// Align stats top-left
			stats.getDomElement().style.position = 'absolute';
			stats.getDomElement().style.left = '0px';
			stats.getDomElement().style.top = '0px';
			document.body.appendChild( stats.getDomElement() );
			setInterval( function () { stats.update(); }, 1000 / 60 );
			// add subtle ambient lighting
	        var ambientLight = new THREE.AmbientLight(0x222222);
	        scene.add(ambientLight);
	        // add directional light source
	        var directionalLight = new THREE.DirectionalLight(0xffffff);
	        directionalLight.position.set(1, 1, 1).normalize();
	        scene.add(directionalLight);
		}
		function getas(a){
			var asfull = ["Alanin","Cystein","Aspartic acid","Glutamic acid","Phenylalanine","Glycine","Histidine","Isoleucine","Lysine","Leucine","Methionine","Asparagine","Proline","Glutamine","Arginine","Serine","Threonine","Valine","Tryptophan","Tyrosine"];
			return asfull[as.indexOf(a)];
		}
		function roundn(x, n){
			if (n < 1 || n > 14) return false;
			var e = Math.pow(10, n);
			var k = (Math.round(x * e) / e).toString();
			if (k.indexOf('.') == -1) k += '.';
			k += e.toString().substring(1);
			return k.substring(0, k.indexOf('.') + n+1);
		}
		function text(t){
			var canvas = document.getElementById('textBox');
		    canvas.width = 200;
		    canvas.height = 200;
		    var context = canvas.getContext("2d");
		    context.textBaseline="top";
		    context.font="50pt Arial";
		    context.fillStyle="black";
		    context.fillText(t,20,20);
		    return canvas;
		}
		//function for axes lines
		function v(x,y,z){ 
	        return new THREE.Vector3(x,y,z); 
	    }
	    //function for sorting AS according to probability
	    function mySorting(a,b) {
			a = a[1];
			b = b[1];
			return a == b ? 0 : (a > b ? -1 : 1)
		}
		//function for doubleclick event
		function DbCl( event ) {
			//get mouse position
			event.preventDefault();
			mouse.x = ( event.clientX / WIDTH ) * 2 - 1;
			mouse.y = - ( event.clientY / HEIGHT ) * 2 + 1;
			var vector = new THREE.Vector3( mouse.x, mouse.y, 0.5 );
			projector.unprojectVector( vector, camera );
			//check for intersection
			var ray = new THREE.Ray( camera.position, vector.subSelf( camera.position ).normalize() );
			var intersects = ray.intersectObjects( objects );
			if ( intersects.length > 0 ) {
				//remove old connections
				if(firstround==false){scene.remove( connections );}
				//get number of intersected object
				nr=(intersects[ 0 ].object.position.x-2)/4;
				$("#dialog").dialog('close');
				sorted = window["asbar"+nr];
				probl=window["probbar"+nr];
				listsort = [];
				//sort and print out AS list in popup
				for(i=0;i<20;i++){
					listsort[i]=[as[i],sorted[i+1],ascol1[i]];
				}
				listsort=listsort.sort(mySorting);
				list="Amino Acid Information:<br>";
				list+="------------------------";
				list+="<table><tr><td>Color</td><td>AA</td><td>Probability</td><td>%</td><td>Full name</td></tr>";
				for(i=0;i<20;i++){
					list=list+"<tr><td><div id='square' style='background-color: #"+listsort[i][2].substr(2)+"; border:1px solid #"+listsort[i][2].substr(2)+"'></div></td><td>"+listsort[i][0]+"</td><td>"+roundn(listsort[i][1],5)+"</td><td>"+roundn(listsort[i][1]*100,2)+"</td><td>"+getas(listsort[i][0])+"</td></tr>";
				}
				list+="</table>";
				list+="------------------------";
				list+="<table><tr><td>From</td><td></td><td>to</td><td>Probability</td><td>%</td></tr>";
				list+="<tr><td>Match</td><td>-></td><td>Match</td><td>"+roundn(probl[21],5)+"</td><td>"+roundn(probl[21]*100,2)+"</td></tr>";
				list+="<tr><td>Match</td><td>-></td><td>Insert</td><td>"+roundn(probl[22],5)+"</td><td>"+roundn(probl[22]*100,2)+"</td></tr>";
				list+="<tr><td>Match</td><td>-></td><td>Deletion</td><td>"+roundn(probl[23],5)+"</td><td>"+roundn(probl[23]*100,2)+"</td></tr>";
				list+="<tr><td>Insert</td><td>-></td><td>Match</td><td>"+roundn(probl[24],5)+"</td><td>"+roundn(probl[24]*100,2)+"</td></tr>";
				list+="<tr><td>Insert</td><td>-></td><td>Insert</td><td>"+roundn(probl[25],5)+"</td><td>"+roundn(probl[25]*100,2)+"</td></tr>";
				list+="<tr><td>Deletion</td><td>-></td><td>Insert</td><td>"+roundn(probl[26],5)+"</td><td>"+roundn(probl[26]*100,2)+"</td></tr>";
				list+="<tr><td>Deletion</td><td>-></td><td>Deletion</td><td>"+roundn(probl[27],5)+"</td><td>"+roundn(probl[27]*100,2)+"</td></tr></table>";
				tit="Position: "+(nr+1);
				$("#dialog").html(list);
				connections = new THREE.Object3D();
				var lineGeo0 = new THREE.Geometry(), lineGeo1 = new THREE.Geometry(), lineGeo2 = new THREE.Geometry();
				if(nr!=last){
					posd = new THREE.Vector3;
					posd = window["del"+nr].position;
					heightd = posd.y*2;
					posi = new THREE.Vector3;
					posi = window["ins"+nr].position;
					heighti = posi.y*2;
					lineGeo0.vertices.push(
						v(intersects[ 0 ].object.position.x, 0, intersects[ 0 ].object.position.z), v(posd.x, heightd, posd.z)	
					);
					lineGeo1.vertices.push(
						v(intersects[ 0 ].object.position.x, 0, intersects[ 0 ].object.position.z), v(posi.x, heighti, posi.z)
					);
					lineGeo2.vertices.push(
						v(intersects[ 0 ].object.position.x, intersects[ 0 ].object.position.y*2*probl[21], intersects[ 0 ].object.position.z), v(intersects[ 0 ].object.position.x+4, intersects[ 0 ].object.position.y*2*window["probbar"+(nr+1)][21], intersects[ 0 ].object.position.z)
					);
				}
				thick = probl[23]*20;
				con = new THREE.Line(lineGeo0, new THREE.LineBasicMaterial({ color: 0x000000, lineWidth: thick }), THREE.Lines);
				connections.add(con);
				thick = probl[22]*20;
				con = new THREE.Line(lineGeo1, new THREE.LineBasicMaterial({ color: 0x000000, lineWidth: thick }), THREE.Lines);
				connections.add(con);
				thick = probl[21]*20;
				con = new THREE.Line(lineGeo2, new THREE.LineBasicMaterial({ color: 0x000000, lineWidth: thick }), THREE.Lines);
				connections.add(con);
				// console.log(window["probbar"+nr]);
				scene.add(connections);
				firstround=false;
				$("#dialog").dialog('open');
				$("#dialog").dialog( "option", "title", tit );
				delete camera;
				delete controls;
				camera = new THREE.PerspectiveCamera( 45, WIDTH / HEIGHT, 1, 10000);
				scene.add( camera );
				camera.position.z = 60;
				camera.position.x = intersects[ 0 ].object.position.x;
				camera.position.y = 5;
				controls = new THREE.CamControls( camera, container );
				targ.x = intersects[ 0 ].object.position.x;
				targ.y = 0;
				targ.z = 0;
				controls.target = targ;
				controls.noZoom = true;
			}
		}
		//animation and render functions for real-time interaction
		function animate(){
			requestAnimationFrame( animate );
			render();
			stats.update();
		}			
		function render(){
			controls.update();
			renderer.render(scene, camera);
		}
	</script>
	<body>
		<?php
		//set used file
		$file=fopen("./test.hmm", "r") or exit("Unable to open file!");
		if($file){
			//arrays for AS and AS sorting
			$hmm = array();
			$as = array(1=>1,2=>2,3=>3,4=>4,5=>5,6=>6,7=>7,8=>8,9=>9,10=>10,11=>11,12=>12,13=>13,14=>14,15=>15,16=>16,17=>17,18=>18,19=>19,20=>20);
			$prob = array(21=>21,22=>22,23=>23,24=>24,25=>25,26=>26,27=>27);
			$hmm["0"]=array("Position","A","C","D","E","F","G","H","I","K","L","M","N","P","Q","R","S","T","V","W","Y","m-m","m-i","m-d","i-m","i-i","d-m","d-d");
			$start=FALSE;
			$length=4;
			$i=1;
			?> <div id="container"></div><br>
<!-- 				<canvas id="textBox"></canvas>​ -->
				<div id="dialog" title="Amino Acid Information">
					<p>List of AS:</p><p>Bar Number: </p><p>AS2</p><p>AS3</p>
				</div>
				<div class="protable">
				<table class="tab">
				<tr class="desc">
					<td>Position</td><td>A</td><td>C</td><td>D</td><td>E</td><td>F</td><td>G</td><td>H</td><td>I</td>
					<td>K</td><td>L</td><td>M</td><td>N</td><td>P</td><td>Q</td><td>R</td><td>S</td><td>T</td><td>V</td	><td>W</td><td>Y</td>
					<td>m-m</td><td>m-i</td><td>m-d</td><td>i-m</td><td>i-i</td><td>d-m</td><td>d-d</td>
				</tr> <?php
			//read file and get values
			while(!feof($file)){
				$line=fgets($file);
				$line=preg_replace('#\s+#',' ',$line);
				$expline=explode(' ',$line);
				if(strcmp(trim($expline[0]),"LENG")==0){ $length=$expline[1]; }
				if($length>=$i){
					if(strcmp(trim($expline[1]),$i)==0){
						$start=TRUE;
						$hmm[$i] = array();
						echo "<tr>";
						for($i2=1;$i2<=21;$i2++){
						if($expline[$i2]!="*" && $expline[$i2]!="0" && $i!=$expline[$i2]){
								if($i%2==0){ echo "<td class='tabfield'>"; }
								else{ echo "<td class='second'>"; } 
								echo round(1/exp($expline[$i2]),5);
								$hmm[$i][$i2-1]=1/exp($expline[$i2]); 
							}
							elseif($i==$expline[$i2]){
								echo "<td class='desc'>";
								echo $expline[$i2];
								$hmm[$i][$i2-1]=$expline[$i2];
							}
							else{
								echo "<td>";
								echo $expline[$i2];
								$hmm[$i][$i2-1]=$expline[$i2];
							}
							echo "</td>";
						}
					}
					if($start==TRUE){
						$expline=explode(' ',$line);
						if(count($expline)<10){
							for($i2=1;$i2<=7;$i2++){
								if($i%2==0){echo "<td class='tabfield'>";}else{echo "<td class='second'>";}
								if($expline[$i2]!="*" && $expline[$i2]!="0"){
									echo round(1/exp($expline[$i2]),5);
									$hmm[$i][20+$i2]=1/exp($expline[$i2]);
								}
								else{
									echo $expline[$i2];
									$hmm[$i][20+$i2]=$expline[$i2];
								}
								echo "</td>";
							}
							echo "</tr>";
							$i++;
							$start=FALSE;
						}
					}
				}
			}
			?> </table></div> <?php
		} ?>
		<script>
			$(document).ready(function(){
				$("div.protable").hide();
				//function for dynamic creation of the axes lines based on HMM length
				function axes(){
					group = new THREE.Object3D();
					var lineGeo = new THREE.Geometry();
					lineGeo.vertices.push(
						v(0, 0, 0), v(<?php echo (count($hmm)-1)*4+5; ?>, 0, 0),
						v(0, 0, 0), v(0, 22, 0),
						<?php $i4=0;
						for($i=0;$i<=count($hmm)-2;$i++){ ?>
							v(<?php echo $i4; ?>, 0, -25), v(<?php echo $i4; ?>, 0, 0),
							<?php $i4 = $i4+4;
						} 
						if($i=count($hmm)-1){ ?>
							v(<?php echo $i4; ?>, 0, -25), v(<?php echo $i4; ?>, 0, 0)
						<?php } ?>
					);
					var lineMat = new THREE.LineBasicMaterial({
					color: 0x000000, lineWidth: 1});
					var line = new THREE.Line(lineGeo, lineMat);
					line.type = THREE.Lines;
					group.add(line);
					scene.add( group );
				}
				function createhmm(){
					//create matches
					<?php $i4=2; for($i=0;$i<=count($hmm)-2;$i++){ ?>
						bar = new THREE.Object3D();
						<?php
						$absheight=0;
						$hmmsort = array();
						reset($hmmsort);
						$hmmsort = $hmm[$i+1];
						$hmmsort = array_intersect_key($hmmsort,$as);
						asort($hmmsort);
						$probgive = array();
						$probgive = array_intersect_key($hmm[$i+1],$prob);
						print "probbar".$i." = ".json_encode($probgive).";";
						print "asbar".$i." = ".json_encode($hmmsort).";";
						for($i2=0;$i2<20;$i2++){ ?>
							col=ascol[<?php echo (key($hmmsort)-1); ?>];
							// col = 0x29d9b3;
							match = new THREE.Mesh( new THREE.CubeGeometry(2,<?php echo current($hmmsort)*20; ?>,2), new THREE.MeshLambertMaterial({ color: col }) );
							match.position.x = <?php echo $i4; ?>;
							match.position.y = <?php if($i2==0){echo current($hmmsort)*20/2;} else{echo $absheight+current($hmmsort)*20/2;} ?>;
							bar.add( match );
							bar.scale.y = <?php echo $hmm[$i+1][21]; ?>;
						<?php $absheight=$absheight+current($hmmsort)*20; next($hmmsort); } ?>
						scene.add( bar );
						matchhit = new THREE.Mesh( new THREE.CubeGeometry(3,20,3), new THREE.MeshBasicMaterial({ color: 0x000000, opacity: 0.25, transparent: true, wireframe: true }) ); 
						matchhit.position.x = <?php echo $i4; $i4=$i4+4; ?>;
						matchhit.position.y = 10;
						matchhit.visible = false;
						scene.add( matchhit );
						objects.push( matchhit );
					<?php } ?>
					//create inserts
					<?php $i4=0; for($i=0;$i<=count($hmm)-3;$i++){
						$insheight = $hmm[$i+1][25]*20; ?>
						ins<?php echo $i; ?> = new THREE.Mesh( new THREE.CubeGeometry(2,<?php echo $insheight; ?>,2), new THREE.MeshLambertMaterial({ color: 0x29d9b3 }) ); 
						ins<?php echo $i; ?>.position.x = <?php $i4=$i4+4; echo $i4; ?>;
						ins<?php echo $i; ?>.position.y = -<?php echo $insheight/2; ?>;
						ins<?php echo $i; ?>.position.z = -16;
						ins<?php echo $i; ?>.rotation.y = 0.8;
						scene.add( ins<?php echo $i; ?> );
					<?php } ?>
					//create deletions
					<?php $i4=2; for($i=0;$i<=count($hmm)-2;$i++){
						$insheight = $hmm[$i+1][27]*20; ?>
						del<?php echo $i; ?> = new THREE.Mesh( new THREE.CylinderGeometry(1,1,<?php echo $insheight; ?>,10), new THREE.MeshLambertMaterial({ color: 0x627364 }) ); 
						del<?php echo $i; ?>.position.x = <?php echo $i4; $i4=$i4+4; ?>;
						del<?php echo $i; ?>.position.y = -<?php echo $insheight/2; ?>;
						del<?php echo $i; ?>.position.z = -8;
						scene.add( del<?php echo $i; ?> );
					<?php if($i==count($hmm)-2){echo "last=".$i; } } ?>
				}
				//get everything going
				init();
				animate();
				axes();
				createhmm();
				// text("match");
				renderer.domElement.addEventListener( 'dblclick', DbCl, false );
				$( "#dialog" ).dialog({ autoOpen: false, position: "left" });
			});
		</script>
	</body>
</html>