/**
 * @author alteredq / http://alteredqualia.com/
 * @author mr.doob / http://mrdoob.com/
 */


Detector = {


  canvas : !! window.CanvasRenderingContext2D,
  webgl : ( function () { try { return !! window.WebGLRenderingContext && !! document.createElement( 'canvas' ).getContext( 'experimental-webgl' ); } catch( e ) { return false; } } )(),
  workers : !! window.Worker,
  fileapi : window.File && window.FileReader && window.FileList && window.Blob,


  getWebGLErrorMessage : function () {


    var domElement = document.createElement( 'div' );


    domElement.style.fontFamily = 'monospace';
    domElement.style.fontSize = '13px';
    domElement.style.textAlign = 'center';
    domElement.style.background = '#eee';
    domElement.style.color = '#000';
    domElement.style.padding = '1em';
    domElement.style.width = '800px';
    domElement.style.margin = '5em auto 0';


    if ( ! this.webgl ) {


      domElement.innerHTML = window.WebGLRenderingContext ? [
        '<p style="color: red"><b>Your browser or your graphics card does not seem to support <a href="http://khronos.org/webgl/wiki/Getting_a_WebGL_Implementation" style="color:#000">WebGL</a>.</b></p><br />',
				'Most common problems:<br><b>Browser related</b><br>We recommend to use <a href="http://www.mozilla.org/de/firefox/new">Mozilla Firefox</a> (>Version 4) or <a href="https://www.google.com/chrome">Google Chrome</a> (>Version 9)<br><b>Firefox related</b><br> On some versions you may have to follow these instructions:<br>Open a new tab<br> Type <i>about:config</i> in the addressbar<br> Click on the <i>I’ll be careful I Promise</i> button<br>Type <i>webgl</i> in the filterbar<br>Doubleclick on <i>webgl.force-enabled</i><br><b>Linux related:</b><br>Direct rendering for your graphics card has to be enabled. To check it open a terminal and type: <i>glxinfo | grep "direct rendering"</i> (glxinfo/mesa package has to be installed).<br> If this returns <i>direct rendering: No</i> you should refer to the manual of your Linux distribution how to enable direct rendering<br><br>',
				'To get additional information please click <a href="http://get.webgl.org/" style="color:#000">here</a>.'
      ].join( '\n' ) : [
        '<p style="color: red"><b>Your graphics card does not seem to support <a href="http://khronos.org/webgl/wiki/Getting_a_WebGL_Implementation" style="color:#000">WebGL</a>.</b></p><br />',
				'Most common problems:<br><b>Browser related</b><br>We recommend to use <a href="http://www.mozilla.org/de/firefox/new">Mozilla Firefox</a> (>Version 4) or <a href="https://www.google.com/chrome">Google Chrome</a> (>Version 9)<br><b>Firefox related</b><br> On some versions you may have to follow these instructions:<br>Open a new tab<br> Type <i>about:config</i> in the addressbar<br> Click on the <i>I’ll be careful I Promise</i> button<br>Type <i>webgl</i> in the filterbar<br>Doubleclick on <i>webgl.force-enabled</i><br><b>Linux related:</b><br>Direct rendering for your graphics card has to be enabled. To check it open a terminal and type: <i>glxinfo | grep "direct rendering"</i> (glxinfo/mesa package has to be installed).<br> If this returns <i>direct rendering: No</i> you should refer to the manual of your Linux distribution how to enable direct rendering<br><br>',
				'To get additional information please click <a href="http://get.webgl.org/" style="color:#000">here</a>.'
      ].join( '\n' );


    }


    return domElement;


  },


  addGetWebGLMessage : function ( parameters ) {


    var parent, id, domElement;


    parameters = parameters || {};


    parent = parameters.parent !== undefined ? parameters.parent : document.body;
    id = parameters.id !== undefined ? parameters.id : 'oldie';


    domElement = Detector.getWebGLErrorMessage();
    domElement.id = id;


    parent.appendChild( domElement );


  }


};
