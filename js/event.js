// events.js
// 
// CLANS Web Viewer, an web application for proteinclassification.
// Copyright (C) 2012 Jens Rauch
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program. If not, see http://www.gnu.org/licenses.

/**
 * @author Jens Rauch
 */

function init(addd){
	// define size
	var WIDTH, HEIGHT, VIEW_ANGLE, ASPECT, NEAR, FAR;
	WIDTH = 1000,
	HEIGHT = 600; 
	// camera settings
	VIEW_ANGLE = 45,
	ASPECT = WIDTH / HEIGHT,
	NEAR = 1,
	FAR = 10000;
	if(addd==true){
		camera = new THREE.PerspectiveCamera( VIEW_ANGLE, ASPECT, NEAR,	FAR);
	}
	scene = new THREE.Scene();
	if(addd==true){
		scene.add( camera );
		renderer = new THREE.WebGLRenderer( { antialias: false } );
		renderer.setSize(WIDTH, HEIGHT);
		projector = new THREE.Projector();
		container = document.getElementById('container');
		container.appendChild(renderer.domElement);
		controls = new THREE.TrackballControls( camera, container );
		camera.position = distance;
		var stats = new Stats();
		// Align top-left
		stats.getDomElement().style.position = 'absolute';
		stats.getDomElement().style.left = '0px';
		stats.getDomElement().style.top = '0px';
		document.body.appendChild( stats.getDomElement() );
		setInterval( function () { stats.update(); }, 1000 / 60 );
	}
}
//functions to toggle show/hide
function hideshow(name){
	var $var = $('div.'+name).children('dl');
	$var.children('dt.title').children('a').click(function(){
		if($var.children('dd.hide').css('display') == "none"){
			$var.children('dd.hide').show('slow');
		}
		else{ $var.children('dd.hide').hide('slow'); }
	});
}
function hideshowm(name){
	var $var = $('div#'+name);
	$var.click(function(){
		if($var.text()=="show tab"){$var.text('hide tab');}
		else{$var.text('show tab');}
		if($var.parent().parent().children('div.hide').css('display') == "none"){
			$var.parent().parent().children('div.hide').show('slow');
		}
		else{ $var.parent().parent().children('div.hide').hide('slow'); }
	});
}
function hideauto(name){
	var $var = $('div#'+name);
		$var.text('show tab');
		$var.parent().parent().children('div.hide').hide();
}
function showauto(name){
	var $var = $('div#'+name);
		if($var.text()!='hide tab'){
			$var.text('hide tab');
			$var.parent().parent().children('div.hide').show();
		}
}
//colorchange background
function hexFromRGB(r, g, b) {
	var hex = [
		r.toString( 16 ),
		g.toString( 16 ),
		b.toString( 16 )
	];
	$.each( hex, function( nr, val ) {
		if ( val.length === 1 ) {
			hex[ nr ] = "0" + val;
		}
	});
	return hex.join( "" ).toUpperCase();
}
function refreshSwatch() {
	var black = $( "#black" ).slider( "value" ),
	color = 255-black,
	hex = hexFromRGB( color, color, color );
	$( "#swatch" ).css( "background-color", "#" + hex );
	$( "#container" ).css( "background-color", "#" + hex );
}
//function to activate the evalue menu and creating the lines in the 3d view
function checkbox(inp) {
	$this=inp;
	if($this.attr('checked')){
		$('.evalb').show();
		//get id
		var nsplit = $this.attr('id').split("-");
		var nr = nsplit[1]-1;
		snr=nr+1;
		ig=0;
		lname=sequence[nr][2].split(" ");
		//add to eval menu
		evaladd = '<table id="table-'+snr+'"><tr class="name" id="name-'+snr+'"><td colspan="3" style="cursor: pointer; width: 100%"><nobr><b>'+(nr+1)+'&nbsp;&#62;'+lname[1]+'</b></nobr></td></tr></table>';
		$(evaladd).appendTo('div.emen');
		//create lines
		for(i=0;i<(sequence[nr].length-3)/2;i++){
			//get grp position
			var gnr = (grpname.indexOf(sequence[nr][4+(ig*2)]+"\n")+1);
			grpnr = window["sphere"+gnr];
			spherenr = window["addedseq"+snr+"a"];
			//add a line to each grp
			window["linemat"+snr+ig] = new THREE.LineBasicMaterial();
			//change color depending on grp
			window["linemat"+snr+ig].color.setRGB(ocolor1[gnr-1],ocolor2[gnr-1],ocolor3[gnr-1]);
			window["line"+snr+ig] = new THREE.Geometry();
			//one point in center of grp
			window["line"+snr+ig].vertices.push(  new THREE.Vertex( grpnr.position ));
			//one point in the new seq point
			window["line"+snr+ig].vertices.push(  new THREE.Vertex( spherenr.position ));
			window["linea"+snr+ig] = new THREE.Line( window["line"+snr+ig], window["linemat"+snr+ig] );
			group.add( window["linea"+snr+ig] );
			reval=Math.pow(10,-sequence[nr][3+(ig*2)]);
			evaladd = '<tr class="line" id="line-'+snr+'"><td class="check" style="width: 10px"><input type="checkbox" id="line-'+snr+"-"+ig+'"/>';
			evaladd += '<td style="width: 10px"><div id="square" style="background-color: #'+grpcolor[gnr-1]+'; border:1px solid #'+grpcolor[gnr-1]+';"></div>';
			evaladd += '</td><td>'+reval.toExponential(1)+' - '+sequence[nr][4+(ig*2)]+'</td></tr>';
			$(evaladd).appendTo('#table-'+snr);
			ig++;
		}
		$('tr#line-'+snr).hide();
		//eventhandler for clicking on names in eval menu
		$('tr.name').off('click');
		$('tr.name').on('click', function(){
			var nsplit = $(this).attr('id').split("-");
			var nr = nsplit[1];
			if($('tr#line-'+nr).css('display') == "none"){ $('tr#line-'+nr).show(); }
			else{ $('tr#line-'+nr).hide(); }
		});	
		//event handler for hovering and checking eval menu entries
		$('tr.line').unbind('mouseenter mouseleave');
		$('tr.line').hover(function(){
			var nsplit = $(this).attr('id').split("-");
			nr = nsplit[1];
			var idsplit = $(this).children('td.check').children('input').attr('id').split("-");
			idnr = idsplit[2];
			mat = "linemat"+nr+idnr;
			if($('input#line-'+nr+'-'+idnr).attr('checked')){}
			else{ window[mat].linewidth=5; }
		}, function(){
			var nsplit = $(this).attr('id').split("-");
			nr = nsplit[1];
			var idsplit = $(this).children('td.check').children('input').attr('id').split("-");
			idnr = idsplit[2];
			mat = "linemat"+nr+idnr;
			if($('input#line-'+nr+'-'+idnr).attr('checked')){}
			else{ window[mat].linewidth=1; }
		});
	}
	else{
		var nsplit = $this.attr('id').split("-");
		var nr = nsplit[1]-1;
		snr=nr+1;
		ig=0;
		for(i=0;i<(sequence[nr].length-2)/2;i++){
			//remove lines
			group.remove(window["linea"+snr+ig]);
			ig++;
		}
		$('#table-'+snr).remove();
	}
	if($('tr.line').data('events')==undefined){ $('.evalb').hide(); $('div.emen').hide('slow'); }
}
function checkgroupscale(){
	var change, factor;
	$('input.pscale').on('change', function(){
		change = $('input.pscale').val();
		factor = change/1;
		group.scale.x = factor;
		group.scale.y = factor;
		group.scale.z = factor;
	});
}

//function for doubleclick event
function DbCl( event ) {
	//get mouse position
	var mouse = new THREE.Vector2();
	var selected = [];
	var WIDTH = window.innerWidth,
		HEIGHT = window.innerHeight;
	event.preventDefault();
	mouse = getMouseInContainer(event.clientX, event.clientY);
	var vector = new THREE.Vector3( mouse.x, mouse.y, 0.5 );
	projector.unprojectVector( vector, camera );
	//check for intersection
	var ray = new THREE.Raycaster( camera.position, vector.sub( camera.position ).normalize() );
	var intersects = ray.intersectObjects( objects );
	if ( intersects.length > 0 ) {
		selected = selected.concat(intersects);
		// get point based on coordinates
		var nr = 0;
		for(var i=0; i<seqpos.length; i++) {
			var psplit = seqpos[i].split(" ");
			if(selected[0].point.x == psplit[1] && selected[0].point.y == psplit[2] && selected[0].point.z == psplit[3]) {
				nr = i;
			}
		}
		// show names if not already displayed
		if($('div.names').children('div.hide').css('display') == "none"){
			$('div.names').children('div.hide').show('slow');
		}
		// check checkbox if unchecked scroll and check it, the checkbox function will do the rest
		if(!$('input#'+nr).attr('checked')){
			$('input#'+nr).trigger('click');
			// scroll to name of the entry
			$('html, body').animate( { scrollTop: $('div#names-hide').offset().top }, 2000);
			$('div.pnames').parent('div.scroll').animate( { scrollTop: $('input#'+nr).offset().top-$('div.pnames').offset().top-10 }, 2000);
		}
		// toggle to remove if already checked
		else {
			$('input#'+nr).trigger('click');
		}
	}
}

// get pos relative to container
getMouseInContainer = function(clientX, clientY) {
	screen = { width: window.innerWidth, height: window.innerHeight, offsetLeft: 0, offsetTop: 0 };
	if (!container) {
		return new THREE.Vector2(
			(clientX - screen.width * 0.5 - screen.offsetLeft) / (screen.width * 0.5),
			(screen.height * 0.5 + screen.offsetTop - clientY) / (screen.height * 0.5));
	}
	var currentElement = container;
	var totalOffsetX = currentElement.offsetLeft - currentElement.scrollLeft;
	var totalOffsetY = currentElement.offsetTop - currentElement.scrollTop;
	var containerY   = 0;
	var containerX	 = 0;
	var canvasY      = 0;
	
	while (currentElement = currentElement.offsetParent) {
		totalOffsetX += currentElement.offsetLeft - $(document).scrollLeft();
		totalOffsetY += currentElement.offsetTop - $(document).scrollTop();
	}
	
	containerX = clientX - totalOffsetX;
	containerY = clientY - totalOffsetY;
	return new THREE.Vector2(
		(containerX - container.offsetWidth * 0.5) / (container.offsetWidth * .5),
		(container.offsetHeight * 0.5 - containerY) / (container.offsetHeight * .5));
};

function setAdvGscale(){
	for(i=1;i<=grpname.length;i++){
		//define vars
		var sph = "sphere"+i;
		var xdata = window[sph].scale.x;
		var ydata = window[sph].scale.y;
		var zdata = window[sph].scale.z;
		//set axes
		$(".sgx").eq(i-1).val(xdata);
		$(".sgy").eq(i-1).val(ydata);
		$(".sgz").eq(i-1).val(zdata);
	}
}

//execute the following as soon as the page is loaded
$(document).ready(function(){
	//reset scroll-pos and checkboxes on reload
	$("html, body").animate({scrollTop:0}, 1);
	//set some init vars for the camera
	var oseqs = 0;
	var col=0;
	hpscale=1;
	point = {};
	firpos=new Array();
	secpos=new Array();
	thipos=new Array();
	link=new Array();
	$selectors=new Array();
	oval=1;
	renderer.domElement.addEventListener( 'dblclick', DbCl, false );
	$('button.deselect-seqs').on('click', function(){
		for(var i=0; i<seqpos.length; i++) {
			if($('input#'+i).attr('checked')){
				$('input#'+i).trigger('click');
			}
		}
	});
	//Hide unneeded info panels and buttons
	$('div.advconf').hide();
	$('.advconfbut').on('click', function(){
		if($('.advconf').css('display')=="none"){ $('.advconf').show(); }
		else{ $('.advconf').hide(); }				
	});
	$('div.info').children('div.hide').hide();
	$('div.addedseqmain').children('div.hide').hide();
	$('div.3dview').children('div.hide').hide();
	$('button.wm').hide();
	$('div.legmen').hide();
	$('.evalb').hide();
	$('div.emen').hide();
	hideauto("names-hide");
	$('button.cam').hide();
	//create about popup
	$( "#dialog" ).dialog({ autoOpen: false, width: 700, position: "top" });
	//show about when clicking on the link
	$( "span.fooright").on('click', function(){
		$("#dialog").dialog('open');
	});
	// init adv group scale
	setAdvGscale();
	//check if post parameters were set and create the corresponding 3d view
	if(window.geths !== undefined){
		addseq("nothing","true",rawfile);
		hideauto("names-hide");
		showauto("addedseq-hide");
	}
	if(window.indexseq !== undefined){
		addseq(seqfield,'false','no');
		hideauto("names-hide");
	}
	if(window.just3d !== undefined){
		var stateObj = { main: "main.php?id="+dirname };
		history.pushState(stateObj, "Test", "main.php?id="+dirname );
		showauto("3dview-hide");
		hideauto("newseq-hide");
	}
	if(window.justid !== undefined){
		showauto("3dview-hide");
		hideauto("newseq-hide");
	}
	//eventhandler for advanced group scale
	$('div.eachgroup').children('input').change(function(){
		//get data
		var axis=$(this).attr("class");
		var i=$(this).attr("id");
		var sph = "sphere"+i;
		var wsp = "wsphere"+i;
		//change scale of spheres
		if(axis=="sgx"){
			var valx=$(this).val();
			window[sph].scale.x = valx;
			window[wsp].scale.x = valx;
		}
		else if(axis=="sgy"){
			var valy=$(this).val();
			window[sph].scale.y = valy;
			window[wsp].scale.y = valy;
		}
		else{
			var valz=$(this).val();
			window[sph].scale.z = valz;
			window[wsp].scale.z = valz;
		}
	});
	//eventhandler for group scale config options
	//input field check
	$('div.gscale').children('input.number').change(function(){
		var valu = $('div.gscale').children('input.number').val();
		if(oval>valu && valu>0){
		var valuc = (valu-oval);
		for(i=1;i<=pgrp;i++){
			var sph = "sphere"+i;
			var wsp = "wsphere"+i;
			window[sph].scale.x += valuc;
			window[sph].scale.y += valuc;
			window[sph].scale.z += valuc;
			window[wsp].scale.x += valuc;
			window[wsp].scale.y += valuc;
			window[wsp].scale.z += valuc;
		}}
		else if(valu<=0){$('div.gscale').children('input.number').val( oval );}
		if(oval<valu && valu>0){
		var valuc = valu-oval;
		for(i=1;i<=pgrp;i++){
			var sph = "sphere"+i;
			var wsp = "wsphere"+i;
			window[sph].scale.x += valuc;
			window[sph].scale.y += valuc;
			window[sph].scale.z += valuc;
			window[wsp].scale.x += valuc;
			window[wsp].scale.y += valuc;
			window[wsp].scale.z += valuc;
		}}
		else if(valu<=0){$('div.gscale').children('input.number').val( oval );}
		oval=$('div.gscale').children('input.number').val();
	});
	//function for groupscalebuttons +/-
	$('div.gscale').children('button.minus').on('click', function(){
		for(i=1;i<=pgrp;i++){
			var sph = "sphere"+i;
			var wsp = "wsphere"+i;
			window[sph].scale.x -= 0.01;
			window[sph].scale.y -= 0.01;
			window[sph].scale.z -= 0.01;
			window[wsp].scale.x -= 0.01;
			window[wsp].scale.y -= 0.01;
			window[wsp].scale.z -= 0.01;
		}
		var newval = $('div.gscale').children('input.number').val();
		newval = parseFloat(newval) - 0.01;
		$('div.gscale').children('input.number').val(Math.round(newval * 100) / 100);
	});
	$('div.gscale').children('button.plus').on('click', function(){
		for(i=1;i<=pgrp;i++){
			var sph = "sphere"+i;
			var wsp = "wsphere"+i;
			window[sph].scale.x += 0.01;
			window[sph].scale.y += 0.01;
			window[sph].scale.z += 0.01;
			window[wsp].scale.x += 0.01;
			window[wsp].scale.y += 0.01;
			window[wsp].scale.z += 0.01;
		}
		var newval = $('div.gscale').children('input.number').val();
		newval = parseFloat(newval) + 0.01;
		$('div.gscale').children('input.number').val(Math.round(newval * 100) / 100);
	});
	//eventhandler for highlighted point scaling
	$('div.hpscale').children('input.number').change(function(){
		var valu = $('div.hpscale').children('input.number').val();
		for(i=0;i<=seqpos.length;i++){
			if(typeof(point[i])!='undefined'){
				point[i].scale.x = valu;
				point[i].scale.y = valu;
				point[i].scale.z = valu;
			}
		}
	});
	//points scalebuttons +/-
	$('div.hpscale').children('button.minus').on('click', function(){
		for(i=0;i<=seqpos.length;i++){
			if(typeof(point[i])!='undefined'){
				point[i].scale.x = parseFloat(point[i].scale.x) - 0.1;
				point[i].scale.y = parseFloat(point[i].scale.y) - 0.1;
				point[i].scale.z = parseFloat(point[i].scale.z) - 0.1;
				$('div.hpscale').children('input.number').val(Math.round(point[i].scale.x * 100) / 100);
			}
		}
		hpscale = hpscale - 0.1;
	});
	$('div.hpscale').children('button.plus').on('click', function(){
		for(i=0;i<=seqpos.length;i++){
			if(typeof(point[i])!='undefined'){
				point[i].scale.x = parseFloat(point[i].scale.x) + 0.1;
				point[i].scale.y = parseFloat(point[i].scale.y) + 0.1;
				point[i].scale.z = parseFloat(point[i].scale.z) + 0.1;
				$('div.hpscale').children('input.number').val(Math.round(point[i].scale.x * 100) / 100);
			}
		}
		hpscale = hpscale + 0.1;
	});
	//eventhandler for fullscreen/window mode
	$('button.fs').on('click', function(){
		$('div#mainb').hide();
		$('.3dview').children('.hide').appendTo('body');
		//update size
		var WIDTH = window.innerWidth,
		HEIGHT = window.innerHeight;
		camera.aspect = WIDTH / HEIGHT;
		renderer.setSize(WIDTH, HEIGHT);
		camera.updateProjectionMatrix();
		var contDiv = document.getElementById("container");
		contDiv.style.position = "absolute";
		contDiv.style.left = "0px";
		contDiv.style.top = "0px";
		contDiv.style.width = WIDTH + "px";
		contDiv.style.height = HEIGHT + "px";
		$('#divbut').css('width', WIDTH + 'px');
		//quick fix for the fullscreenmode with the scrollbars
		$('.leg').css('top', '85px');
		$('.legmen').css('top', '115px');
		$('.cam').css('top', '535px');
		eheight=HEIGHT-50;
		ewidth=WIDTH-140;
		lheight=eheight-200;
		lwidth=ewidth-30;
		$('.evalb').css('top', eheight+'px');
		$('.evalb').css('left', ewidth+'px');
		$('.emen').css('top', lheight+'px');
		$('.emen').css('left', lwidth+'px');
		$('button.wm').show();
		$('button.fs').hide();
	});
	$('button.wm').on('click', function(){
		$('#conthide').appendTo('.3dview');
		$('div#mainb').show();
		//update size
		var WIDTH = 1000,
		HEIGHT = 600;
		camera.aspect = WIDTH / HEIGHT;
		renderer.setSize(WIDTH, HEIGHT);
		camera.updateProjectionMatrix();
		var contDiv = document.getElementById("container");
		contDiv.style.width = WIDTH + "px";
		contDiv.style.height = HEIGHT + "px";
		contDiv.style.position = "static";
		$('#divbut').css('width', WIDTH + 'px');
		//quick fix for the fullscreenmode with the scrollbars
		$('.cam').css('top', '485px');
		$('.leg').css('top', '35px');
		$('.legmen').css('top', '65px');
		$('.evalb').css('top', '565px');
		$('.evalb').css('left', '880px');
		$('.emen').css('top', '365px');
		$('.emen').css('left', '845px');
		$('button.wm').hide();
		$('button.fs').show();
	});
	//initialize Tooltip
	var tooltip = $('<div class="tooltip"></div>').text('');
	$(tooltip).appendTo('body');
	//show tooltips
	$('#newseqinfo').mouseenter(function(){
		tooltip.html('You can past your FASTA format sequences in the field below. If you press \"Add\" they will be compared via HMM models with the groups shown below. The added sequences will show up as red squares.<br>See manual chapter 1.2.4')
		.css({
			top: $(this).position().top+30,
			left: $(this).position().left+10
		})
		.fadeIn('slow'); })
		.mouseleave(function(){
			tooltip.fadeOut('slow');
	});
	$('#3dviewinfo').mouseenter(function(){
		tooltip.html('You can see the 3D view of the selected CLANS file here. You can select points by double-clicking.<br>See manual chapter 1.2.3')
		.css({
			top: $(this).position().top+30,
			left: $(this).position().left+10
		})
		.fadeIn('slow'); })
		.mouseleave(function(){
			tooltip.fadeOut('slow');
	});
	$('#addedseqinfo').mouseenter(function(){
		tooltip.html('You can see your added sequences here. The number in the bar represents the E-Value. The lower it is the similar your sequence is to the compared group.<br>See manual chapter 1.2.5')
		.css({
			top: $(this).position().top+30,
			left: $(this).position().left+10
		})
		.fadeIn('slow'); })
		.mouseleave(function(){
			tooltip.fadeOut('slow');
	});
	$('#clansinfo').mouseenter(function(){
		tooltip.html('You can see all the sequences and groups in the original CLANS file below. By checking the box you can highlight specific points in the 3D view as a big black sphere.<br>See manual chapter 1.2.6')
		.css({
			top: $(this).position().top+30,
			left: $(this).position().left+10
		})
		.fadeIn('slow'); })
		.mouseleave(function(){
			tooltip.fadeOut('slow');
	});
	$('.bookmark').mouseenter(function(){
		tooltip.html('If you want to access this website directly you can just click here and bookmark it for easier navigation.<br>See manual chapter 1.2.2.3')
		.css({
			top: $(this).position().top+30,
			left: $(this).position().left+10
		})
		.fadeIn('slow'); })
		.mouseleave(function(){
			tooltip.fadeOut('slow');
	});
	$('#loginfo').mouseenter(function(){
		tooltip.html('The info & config tab shows additional configuration options, information and errors.<br>See manual chapter 1.2.7')
		.css({
			top: $(this).position().top-50,
			left: $(this).position().left+10
		})
		.fadeIn('slow'); })
		.mouseleave(function(){
			tooltip.fadeOut('slow');
	});
	$('.legmenhelp').mouseenter(function(){
		tooltip.html('You can use the M ("Mark") and S ("Show") checkboxes to hide or mark specific groups.<br>See manual chapter 1.2.3.3')
		.css({
			top: 210+30,
			left: 140+10
		})
		.fadeIn('slow'); })
		.mouseleave(function(){
			tooltip.fadeOut('slow');
	});
	//colorchange page
	csslink = $('link').eq(0);
	$('span#colchange').on('click', function(){
		stylesheet = $(this).data('file');
		csslink.attr('href', '../css/'+stylesheet + '.css');
	});
	//colorchange background
	$( "#black" ).slider({
		orientation: "horizontal",
		range: "min",
		max: 255,
		value: 13,
		slide: refreshSwatch,
		change: refreshSwatch
	});
	$( "#black" ).slider( "value", 13 );
	//bookmarkfunction
	$('.bookmark').on('click', function(){
		alert('Due to technical reasons please press ctrl+D to bookmark (Command+D for macs)');
	});
	//show3d button handler
	$('.show3d').on('click', function(){
		showauto("3dview-hide");
		scrollTo(0,0);
	});
	//eventhandler to show panels on click
	hideshowm("newseq-hide");
	hideshowm("3dview-hide");
	hideshowm("addedseqtab-hide");
	hideshowm("names-hide");
	hideshowm("info-hide");
	//show legend menu on click
	$('button.leg').on('click', function(){
		if($('div.legmen').css('display') == "none"){ $('div.legmen').show('slow'); $('button.cam').show(); }
		else{ $('div.legmen').hide('slow'); $('button.cam').hide(); }
	});
	//show eval menu on click
	$('button.evalb').on('click', function(){
		if($('div.emen').css('display') == "none"){ $('div.emen').show('slow'); }
		else{ $('div.emen').hide('slow'); }
	});
	//disable camera turing while hovering the eval menu
	//deactivate camera controls while hovering the menu
	$('div.emen').hover(function(){
		controls.enabled=false;
	}, function(){
		controls.enabled=true;
	});
	//reset camera to default on click
	$('button.cam').on('click', function(){
		scene.remove( camera );
		delete camera;
		delete controls;
		WIDTH = 1000,
		HEIGHT = 600; 
		VIEW_ANGLE = 45,
		ASPECT = WIDTH / HEIGHT,
		NEAR = 1,
		FAR = 10000;
		camera = new THREE.PerspectiveCamera( VIEW_ANGLE, ASPECT, NEAR,	FAR);
		scene.add( camera );
		camera.position = DEFDIST;
		controls = new THREE.TrackballControls( camera, container );
	});
	// eventhandler for proteinames
	$('div.pointcheck').on('change', 'input', function(event){
		if($(this).attr('checked')){
			//get id
			var nr = $(this).attr('id');
			var psplit = seqpos[nr].split(" ");
			//create point in 3d view
			point[nr] = new THREE.Mesh( new THREE.SphereGeometry(0.1,10,10), new THREE.MeshBasicMaterial({ color: 0x000000 }) );
			point[nr].position.x = psplit[1];
			point[nr].position.y = psplit[2];
			point[nr].position.z = psplit[3];
			point[nr].scale.x = hpscale;
			point[nr].scale.y = hpscale;
			point[nr].scale.z = hpscale;
			group.add(point[nr]);
			$this=$('div.pointcheck');
			if($(this).attr('checked') && $('div.hpscale').children('input.number').attr('disabled')=='disabled'){
				$this = $('div.hpscale');
				$this.children('input.number').removeAttr('disabled');
				$this.children('button.minus').removeAttr('disabled');
				$this.children('button.plus').removeAttr('disabled');
			}
		}
		else{
			var nr = $(this).attr('id');
			group.remove(point[nr]);
			if($(this).attr('checked')!=true){
				$this = $('div.hpscale');
				$this.children('input.number').attr('disabled', 'disabled');
				$this.children('button.minus').attr('disabled', 'disabled');
				$this.children('button.plus').attr('disabled', 'disabled');
			}
		}
	});
	// eventhandler for the legend menu
	var hidesphere = new Array();
	//deactivate camera controls while hovering the menu
	$('div.legmen').hover(function(){
		controls.enabled=false;
	}, function(){
		controls.enabled=true;
	});
	//hovering in legend highlights groups
	$('tr.grp').hover(function(){
		//get id
		var nsplit = $(this).attr('id').split("-");
		nr = nsplit[1];
		//toggle wireframe
		mat = "material"+nr;
		wsp = "wsphere"+nr;
		grp="grp-"+nr;
		hgrp="hidegrp-"+nr;
		(window[mat]).color.setRGB(0,0,0);
		if($('input.'+grp).attr('checked')){}
		else{ group.add( window[wsp] ); }
	}, function(){
		window[mat].color.setRGB(ocolor1[nr-1],ocolor2[nr-1],ocolor3[nr-1]);
		var nsplit = $(this).attr('id').split("-");
		nr = nsplit[1];
		if($('input.'+grp).attr('checked')){}
		else{ group.remove( window[wsp] ); }
		if($('input.'+hgrp).attr('checked') && hidesphere[nr]==true){
			group.add( window["sphere"+nr] );
			hidesphere[nr]=false;
		}
		else if($('input.'+hgrp).attr('checked')!="checked" && hidesphere[nr]!=true){
			group.remove( window["sphere"+nr] );
			hidesphere[nr]=true;
		}
	});
	//checking the hideall checkboxe
	$('input.hideall').on('change', function(){
		if($('input.hideall').attr('checked')!="checked"){
			for(i=1;i<=grpname.length;i++){
				hgrp="hidegrp-"+i;
				if($('input.'+hgrp).attr('checked')=="checked"){
					$('input.'+hgrp).prop("checked", false);
					group.remove( window["sphere"+i] );
					hidesphere[i]=true;
				}
			}
		}
		else{
			for(i=1;i<=grpname.length;i++){
				hgrp="hidegrp-"+i;
				if($('input.'+hgrp).attr('checked')!="checked"){
					$('input.'+hgrp).prop("checked", "checked");
					group.add( window["sphere"+i] );
					hidesphere[i]=false;
				}
			}
		}
	});
	//checking the markall checkboxe
	$('input.markall').on('change', function(){
		if($('input.markall').attr('checked')!="checked"){
			for(i=1;i<=grpname.length;i++){
				grp="grp-"+i;
				wsp = "wsphere"+i;
				if($('input.'+grp).attr('checked')=="checked"){
					$('input.'+grp).prop("checked", false);
					group.remove( window[wsp] );
				}
			}
		}
		else{
			for(i=1;i<=grpname.length;i++){
				grp="grp-"+i;
				wsp = "wsphere"+i;
				if($('input.'+grp).attr('checked')!="checked"){
					$('input.'+grp).prop("checked", "checked");
					group.add( window[wsp] );
				}
			}
		}
	});
	//Save and read data for advanced config
	$getinput = $('input.getfile');
	$getinput.change(function(){
		var val2 = $getinput.val();
		if(val2.length>0){ $('input.readsize').removeAttr('disabled'); }
		else{ $('input.readsize').attr('disabled', 'disabled'); }
	});
	$("button.savesize").click(function(){
		var data = "";
		var $sgx = $(".sgx");
		var $sgy = $(".sgy");
		var $sgz = $(".sgz");
		for(i=0;i<grpname.length;i++){
			data += "group "+i+"\n";
			data += $sgx.eq(i).val()+"\n";
			data += $sgy.eq(i).val()+"\n";
			data += $sgz.eq(i).val()+"\n";
		}
		cdata = $.ajax({
			type: 'POST',
			async: false,
			url: '../back/fileIO.php',
			data: ({
				id: "1",
				data: data
			})
		}).responseText;
    	window.open("../saved_files/"+cdata,"Download");
    	setTimeout(function(){
			$.ajax({
				type: 'POST',
				async: false,
				url: '../back/fileIO.php',
				data: ({
					id: "3",
					data: cdata
				})
			});
		}, 1000);
	});
	function checkfordata(){
		if(typeof(getdata)=='undefined'){setTimeout(function() {checkfordata()}, 500);}
		else{
			for(i=1;i<=grpname.length;i++){
				//define vars
				var sph = "sphere"+i;
				var wsp = "wsphere"+i;
				var xdata = getdata[((i-1)*4)+1];
				var ydata = getdata[((i-1)*4)+2];
				var zdata = getdata[((i-1)*4)+3];
				//set x axis
				$(".sgx").eq(i-1).val(xdata);
				window[sph].scale.x = valx;
				window[wsp].scale.x = valx;
				//set y axis
				$(".sgy").eq(i-1).val(ydata);
				window[sph].scale.y = valy;
				window[wsp].scale.y = valy;
				//set z axis
				$(".sgz").eq(i-1).val(zdata);
				window[sph].scale.z = valz;
				window[wsp].scale.z = valz;
			}
		}
	}
	$("input.readsize").click(function(){
		checkfordata();
	});	
	//focusing a group by clicking on the name
	$('td.legname').click(function(){
		var nsplitc = $(this).attr('id').split("-");
		nrc = nsplitc[1];
		snrc = parseFloat(nrc)+1;
		spnrc = "sphere"+snrc;
		distance.x = window[spnrc].position.x;
		distance.y = window[spnrc].position.y;
		distance.z = window[spnrc].position.z + 20;
		scene.remove( camera );
		delete camera;
		delete controls;
		WIDTH = 1000,
		HEIGHT = 600; 
		VIEW_ANGLE = 45,
		ASPECT = WIDTH / HEIGHT,
		NEAR = 1,
		FAR = 10000;
		camera = new THREE.PerspectiveCamera( VIEW_ANGLE, ASPECT, NEAR,	FAR);
		scene.add( camera );
		camera.position = distance;
		controls = new THREE.TrackballControls( camera, container );
		controls.target = window[spnrc].position;
		controls.enabled=false;
		$('div.legmen').on('mouseleave', function(){
			controls.enabled=true;
			$('div.legmen').unbind('mouseenter mouseleave');
			$('div.legmen').hover(function(){
				controls.enabled=false;
			}, function(){
				controls.enabled=true;
			});
		});
	});
	//activate "add" button if a sequence is pasted (keyboard)
	$('#addfasta').keyup(function(){
		if($('#addfasta').val().length>0){ $('#addfastab').removeAttr('disabled'); }
		else{ $('#addfastab').attr('disabled', 'disabled'); }
	});
	//activate "add" button if a sequence is pasted
    $("#addfasta").bind('paste', function(e) {
		$('#addfastab').removeAttr('disabled');
	});
	//add seq handler
	//call sequence.php with POST when clicking on the add button
	$('dd.addseq').children('button').on('click', function(){
		seqfield = $('dd.addseq').children('textarea').val();
		var second = false;
		//if no name is specified add a dummy name:
		testseqname=new Array();
		testseqname=seqfield.split("");
		if(testseqname[0]!=" " && testseqname[0]!=">"){
			seqfield="> dummy name\n"+seqfield;
		}
		else if(testseqname[0]!=" "){
			for(i=0;i<10;i++){
				if(testseqname[0]!=" " && testseqname[0]!=">"){
					addedseq="> dummy name\n".addedseq;
				}
			}
		}
		var addedseqpost="";
		seqfieldsplit = seqfield.split("\n");
		for(si=0;si<seqfieldsplit.length;si++){
			testseq = seqfieldsplit[si].split("");
			//remove non ASCI characters
			if(testseq[0]==">"){
				if(second == true){
					seqfieldsplit[si] = seqfieldsplit[si].replace(/[^(\x20-\x7F)]*/g, '');
					addedseqpost += "\n"+seqfieldsplit[si]+"\n";
				}
				else{
					seqfieldsplit[si] = seqfieldsplit[si].replace(/[^(\x20-\x7F)]*/g, '');
					addedseqpost += seqfieldsplit[si]+"\n";
				}
			}
			else{
				if(seqfieldsplit[si]!=" "){
					seqfieldsplit[si] = seqfieldsplit[si].replace(/[^(\x20-\x7F)]*/g, '');
					addedseqpost += seqfieldsplit[si];
				}
			}
		}
		addseq(addedseqpost,'false','no');
	});
	//this function is called if the addfasta script finished (add seq from text file)
	window.finish = function(seqfield){
		addseq(seqfield,"false","no");
	};
	//activate button if file is specified
	$input2 = $('input.file');
	$input2.change(function(){
		var val2 = $input2.val();
		if(val2.length>0){ $('input.sequpb').removeAttr('disabled'); }
		else{ $('input.sequpb').attr('disabled', 'disabled'); }
	});
	//function for adding sequences		
	function addseq(seqfield, wfile, wfilename){
		pass = $.ajax({
			type: 'POST',
			async: false,	// wait for the script to execute before doing anything else
			url: '../back/sequence.php',
			data: ({
				a: seqfield,
				b: dirnameseq,
				c: wfile,
				d: wfilename,
				e: "seqadd"
			})
		}).responseText;
		//pass the output of the php file to the results array
		if(pass.indexOf("!!Error!!") != -1){alert("Your Sequence is not in FASTA format! Please Check it!");}
		else{
			$('dd.addseq').children('textarea').val("");
			result=pass.split("\n");
			if(typeof(sequence)=='undefined'){sequence=new Array();}
			if(typeof(positions)=='undefined'){positions=new Array();}
			seqc=result.length-1;
			diff=0;
			comdiff=0;
			i=0;
			compl=new Array();
			minl=new Array();
			//split the results into an multidimensional array so every sequence is separated
			while(seqc>0){
			    pnr = (oseqs+i);
			    compl[i]=0;
			    minl[i]=-1;
			    diff=(3+result[comdiff]*2);
			    if(typeof(sequence[pnr])=='undefined'){ sequence[pnr]=new Array(); }
				for(ix=0;ix<=diff;ix++){
				    sequence[pnr][ix]=result[comdiff+ix];
				}
				comdiff=comdiff+diff;
				seqc=seqc-diff;
				sequence[pnr].pop();
				for(ix=0;ix<(sequence[pnr].length-3)/2;ix++){
				    if(minl[i]==-1){
				    	minl[i]=parseFloat(sequence[pnr][3+(ix*2)]);
				    }
				    else{
					    tmp=parseFloat(sequence[pnr][3+(ix*2)]);
					    if(minl[i]>tmp){
					        minl[i]=tmp;
					    }
				    }
				}
				for(ix=0;ix<(sequence[pnr].length-3)/2;ix++){
				    //and if I want to use a squaring or cubing of the values:
					tmp=(parseFloat(sequence[pnr][3+(ix*2)])-parseFloat(minl[i]));
				    compl[i] = compl[i] + tmp*tmp*tmp;
				}
				i++;
			} 
			var seqlen=i;
			$('.hidetextseq').hide();
			$('div.addedseqmain').children('div.hide').show();
			$('#addedseqtab-hide').text('hide tab');
			hideauto("newseq-hide");
			//get coordinates for groups which are in the hmmer file
			for(i=0;i<seqlen;i++){
				pnr = (oseqs+i);
				snr = (oseqs+i+1);
				if(sequence[pnr][3]=="Warning:"){
					$selectors[gnr] = $("<div class='seqnamemain' style='background: black'><input type='hidden' class='egrpname' id='seq-"+snr+"'/><span class='realseqname'>"+snr+"&nbsp;&#62;"+sequence[pnr][2]+"</span></div>").appendTo('div.pnamesadded');
					evalbars='<div class="hideall" id="h-'+snr+'">';
					evalbars+='<span class="grpnr">'+sequence[pnr][4]+'</span></div>';
					evalbars+='<div class="bottom"></div></div>';
					$(evalbars).appendTo('div.pnamesadded');
					if(typeof(positions[pnr])=='undefined'){positions[pnr]=new Array();}
				}
				else{
					var gnr = (grpname.indexOf(sequence[pnr][4]+"\n")+1);
					//get color of the best hit
					var gcolor = grpcolor[(grpname.indexOf(sequence[pnr][4]+"\n")+1)-1];
					//add the names to the proteinnames part of the website
					$selectors[gnr] = $("<div class='seqnamemain' style='background: #"+gcolor+"'><input type='checkbox' class='egrpname' id='seq-"+snr+"'/><span class='realseqname'>"+snr+"&nbsp;&#62;"+sequence[pnr][2]+"</span></div>").appendTo('div.pnamesadded');
					newx=0,
					newy=0,
					newz=0;
					if(typeof(positions[pnr])=='undefined'){positions[pnr]=new Array();}
					perc=new Array();
					realperc=new Array();
					entropy = 0;
					//calculate the new point coordinates
					for(i2=0;i2<(sequence[pnr].length-3)/2;i2++){
						//get grpnumber & position
						var gnr = (grpname.indexOf(sequence[pnr][4+(i2*2)]+"\n")+1);
						spherenr = window["sphere"+gnr];
						positions[pnr][i2] = new THREE.Vector3();
						positions[pnr][i2] = spherenr.position;
						//get percentage contribution of each Evalue to the sequence positioning
						tmp=(sequence[pnr][3+(i2*2)]-minl[i]);
						tmpval=tmp*tmp*tmp;
						realperc[i2] = (tmpval/compl[i])*100;
						pval = tmpval/compl[i];
						entropy += pval*Math.log(pval);
						//fixes for drawing
						//try a square/3rd power of the evalues
						perc[i2] = tmpval/compl[i];
						//calculate the average of the groups
						newx = newx+spherenr.position.x;
						newy = newy+spherenr.position.y;
						newz = newz+spherenr.position.z;
                    } 
					entropy = -entropy;
					newx=newx/((sequence[pnr].length-3)/2);
					newy=newy/((sequence[pnr].length-3)/2);
					newz=newz/((sequence[pnr].length-3)/2);
					//make the avarage a vector
					aspos=new THREE.Vector3();
					aspos.x=newx;
					aspos.y=newy;
					aspos.z=newz;
					evalbars='<div class="hideall" id="h-'+snr+'">';
					//create new variable from realperc[0] to check if the percentage has to be multiplied
					checkper=realperc[0];
					for(i2=0;i2<(sequence[pnr].length-3)/2;i2++){
						vec1=new THREE.Vector3();
						//calculate the orientationvector
						vec1.set((positions[pnr][i2].x-aspos.x),(positions[pnr][i2].y-aspos.y),(positions[pnr][i2].z-aspos.z));
						vec2=vec1.clone();
						vec3 = vec2.multiplyScalar(perc[i2]);
						//shift ovector to point
						if(i2==0){ vec3.add(aspos); }
						else{ vec3.add(vec4); }
						vec4=vec3.clone();
						//multiply perc by factor to get maximum of 1000px width
						if(checkper<10){
							realperc[i2]=realperc[i2]*10;
						}
						else if(checkper<25 && checkper>10){
							realperc[i2]=realperc[i2]*4;
						}
						else if(checkper<50 && checkper>25){
							realperc[i2]=realperc[i2]*2;
						}
						realperc[i2]=realperc[i2]*9.8;
						//add evalues in menu while on mainpage
						var gnr = (grpname.indexOf(sequence[pnr][4+(i2*2)]+"\n")+1);
						reval=Math.pow(10,-sequence[pnr][3+(i2*2)]);
						if(i2<=5){
							evalbars += '<span class="grpnr">'+sequence[pnr][4+(i2*2)]+'</span><br><div class="evalmain" style="width: '+realperc[i2]+'px; height: 15px; background-color: #'+grpcolor[gnr-1]+'; border:1px solid #'+grpcolor[gnr-1]+'">'+reval.toExponential(1)+'</div>';
						}
						else if(i2==6){
							evalbars += '<div class="hideall" id="hb-'+snr+'">';
							evalbars += '<span class="grpnr">'+sequence[pnr][4+(i2*2)]+'</span><br><div class="evalmain" style="width: '+realperc[i2]+'px; height: 15px; background-color: #'+grpcolor[gnr-1]+'; border:1px solid #'+grpcolor[gnr-1]+'">'+reval.toExponential(1)+'</div>';
						}
						else if(i2>6){
							evalbars += '<span class="grpnr">'+sequence[pnr][4+(i2*2)]+'</span><br><div class="evalmain" style="width: '+realperc[i2]+'px; height: 15px; background-color: #'+grpcolor[gnr-1]+'; border:1px solid #'+grpcolor[gnr-1]+'">'+reval.toExponential(1)+'</div>';
						}
						//add the last point to the scene
						if(i2==((sequence[pnr].length-3)/2)-1){
							window["addedseq"+snr+"a"] = new THREE.Mesh( new THREE.CubeGeometry(0.2,0.2,0.2), new THREE.MeshBasicMaterial({ color: 0xCC3300 }) ); 
							window["addedseq"+snr+"a"].position = vec4;
							group.add( window["addedseq"+snr+"a"] );
							if(i2<=5){
								evalbars+='<div class="bottom"></div></div>'; 
							}
							else{ evalbars+='</div><div class="bottom"></div></div>'; }
							$(evalbars).appendTo('div.pnamesadded');
							$('div#hb-'+snr).hide();
						}
					}
				}
				//refresh eventhandler for buttons
				$('button.collapsall').off('click');
				$('button.collapsall').on('click', function(){
					$('div.hideall').hide();
					$(this).attr("disabled", "disabled");
					if($('.showall').attr("disabled")=="disabled"){$('.showall').removeAttr('disabled');}
				});
				$('button.showall').off('click');
				$('button.showall').on('click', function(){
					$('div.hideall').show();
					$(this).attr("disabled", "disabled");
					if($('.collapsall').attr("disabled")=="disabled"){$('.collapsall').removeAttr('disabled');}
				});
				$('button.select').off('click');
				$('button.select').on('click', function(){
					$('div.seqnamemain').children('input').each(function(){
						if($(this).attr('checked')!="checked"){
							$(this).attr('checked', true);
							checkbox($(this));
						}
					});
				});
				$('button.deselect').off('click');
				$('button.deselect').on('click', function(){
					$('div.seqnamemain').children('input').each(function(){
						if($(this).attr('checked')){
							$(this).attr('checked', false);
							checkbox($(this));
						}
					});
				});
				$seqnamemain = $('.realseqname');
				$seqnamemain.off('click');
				$seqnamemain.on('click', function(){
					var ssplit = $(this).parent().children('input').attr('id').split("-");
					if($.trim($('div#hb-'+ssplit[1]).text()) == ""){
						if($('div#h-'+ssplit[1]).css('display')=="none"){
							$('div#h-'+ssplit[1]).show();
							$('div#hb-'+ssplit[1]).show();
						}
						else{
							$('div#h-'+ssplit[1]).hide();
							$('div#hb-'+ssplit[1]).hide();
						}
					}
					else{
						if($('div#hb-'+ssplit[1]).css('display')=="none"){
							$('div#h-'+ssplit[1]).show();
							$('div#hb-'+ssplit[1]).show();
						}
						else{
							$('div#h-'+ssplit[1]).hide();
							$('div#hb-'+ssplit[1]).hide();
						}
					}
				});
				if($('.showall').attr("disabled")=="disabled"){$('.showall').removeAttr('disabled');}
				if($('.collapsall').attr("disabled")=="disabled"){$('.collapsall').removeAttr('disabled');}
				if($('.deselect').attr("disabled")=="disabled"){$('.deselect').removeAttr('disabled');}
				if($('.select').attr("disabled")=="disabled"){$('.select').removeAttr('disabled');}
				//check if multiple output files exist and add merge files is necessary
				if(typeof(linkname)=="undefined"){
					linknr=0;
					slink = sequence[pnr][1].split("/");
					linkname = slink[slink.length-1];
					for(il=0;il<slink.length;il++){
						if(slink[il]=="saved_files"){
							link[0]="../"+slink[il];
						}
						else if(typeof(link[0])!='undefined'){ link[0] += "/"+slink[il]; }
						if(il==slink.length-1){linkname=slink[il];}
					}
					$('button.rawoutput').on('click', function(){
						window.open( link[0], '_blank' );
					});
					var stateObj = { main: "main.php?hs="+linkname+"&id="+dirname };
					history.pushState(stateObj, "Test", "main.php?hs="+linkname+"&id="+dirname );
					rawfile=linkname;
				}
				else{
					slink = sequence[pnr][1].split("/");
					if(linkname!=slink[slink.length-1]){
						pass2 = $.ajax({
							type: 'POST',
							async: false,	// wait for the script to execute before doing anything else
							url: '../back/sequence.php',
							data: ({
								a: "false",
								b: dirnameseq,
								c: slink[slink.length-1],
								d: rawfile,
								e: "addtofile"
							})
						}).responseText;
					}
					linkname = slink[slink.length-1];
				}
			}
			oseqs = sequence.length;
			$inputfield=$('div.seqnamemain').children('input');
			$inputfield.off('change');
			$inputfield.on('change', function(){
				checkbox($(this));
			});
		}
	}
});