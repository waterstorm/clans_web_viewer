<!doctype html>
<!-- main.php

CLANS Web Viewer, an web application for proteinclassification.
Copyright (C) 2012 Jens Rauch

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see http://www.gnu.org/licenses. -->
<html>
	<head>
		<meta charset="utf-8" />
		<title>CLANS Web Viewer (Beta)</title>
		<link rel="stylesheet" href="../css/main_green.css">
		<script src="../js/jquery-1.7.2.min.js" type="text/javascript" charset="utf-8"></script>
		<script src="../js/three_59.min.js" type="text/javascript" charset="utf-8"></script>
		<script src="../js/TrackballControls.js" type="text/javascript" charset="utf-8"></script>
		<script src="../js/Raycaster.js" type="text/javascript" charset="utf-8"></script>
		<script src="../js/Stats.js" type="text/javascript" charset="utf-8"></script>
		<script src="../js/Detector.js" type="text/javascript" charset="utf-8"></script>
		<script src="../js/event.js" type="text/javascript" charset="utf-8"></script>
		<link type="text/css" href="../css/trontastic/jquery-ui-1.8.21.custom.css" rel="stylesheet" />
		<script type="text/javascript" src="../js/jquery-ui-1.8.21.custom.min.js"></script>
		<script>
			function animate(){
				requestAnimationFrame( animate );
				controls.update();
				render();
			}			
			function render(){
				renderer.render(scene, camera);
			}
		</script>
	</head>
	<body>
		<?php
		$ddir = "../saved_files/";
		//include the backend and check for correct id
			if(empty($_POST["id"])==FALSE or empty($_GET["id"])==FALSE){
				if(empty($_POST["id"])==FALSE){$dirname = $_POST["id"];}
				if(empty($_GET["id"])==FALSE){$dirname = $_GET["id"];}
				if(empty($_GET["hs"])==FALSE){$rawfile = $_GET["hs"];}
				if(empty($_POST["groups"])==FALSE){$showgrps = $_POST["groups"];}
				if(empty($_POST["size"])==FALSE){$showsize = $_POST["size"];}
				$filename = $dirname.".txt";
				$fpath = $ddir.$filename;
				$out = array();
				exec("ls ".$ddir.$dirname." | grep .hmm 2>&1", $out);
				$hmmavail = count($out);
				if(file_exists($fpath)==TRUE){
					include("../back/parsearray.php");
					include("../back/initparticle.php");
					if(empty($_POST["addfasta"])==FALSE){ $addedseq = $_POST["addfasta"]; }
				}
				else{ echo "<h1><span style='color: red;'><b>This is not a valid ID! Please visit the <a href='../index.php'>index.php</a></span></h1></b>"; }
			}
			else{ echo "<h1><span style='color: red;'><b>You should not be here! Please visit the <a href='../index.php'>index.php</a></span></h1></b>"; }
		?>
	<div id="mainb">
		<div id="header">
			<span class="htext"><a href="../index.php" class="htext">CLANS Web Viewer</a></span><span class="htextadd">Beta</span>
			<span class="bookmark"><b>Bookmark</b> for direct access</span>
		</div>
		<div id="mainsite">
			<?php include("./about.html"); ?>
			<div class="tooltip"></div>
			<div class="3dview">
				<div id="headw"><div id="3dview-hide" class="headtextmenu">show tab</div><a href="../clans_web_viewer-help.pdf#page=6" target="_blank"><div id="3dviewinfo" class="infopic"></div></a><div class="headtext">3D View:</div></div>
				<div class="hide" id="conthide">
					<div id="container">
						<div class="errormessage"><b><h2><span style="color: red">Please read this note: <br />If your browser is still loading the page (turning circle/loading bar) just wait a few minutes. If nothing happens, your WebGL is probably disabled. Most likely because "direct rendering" is disabled on your graphics card/browser. You have to enable it to use this site. You can get further informations in the <a href="../clans_web_viewer-help.pdf#page=14" target="_blank">FAQ of this web application</a> (see manual chapter 3.1)</span></h2></b></div>
						<div id="wrapper">
							<button class="fs">fullscreen</button>
							<button class="wm">window</button>
							<button class="leg">Toggle Legend</button>
							<button class="cam">Reset camera</button>
							<div class="legmen">
								<table style="border-bottom: 1px solid black; width: 100%">
									<tr class="legmenhead">
										<td class="legmenhelp" align="center" style="width: 42px">M&nbsp;&nbsp;S</td>
										<td class="legmenname" colspan="2">Color + Name</td>
									</tr>
									<tr>
										<td>
										<nobr><input type="checkbox" class="markall"/><input type="checkbox" checked="checked" class="hideall" /></nobr>
										</td>
										<td id="square"></td>
										<td align="left">
											Select all
										</td>
									</tr>
								</table>
								<div class="scrolltable">
									<table>
										<?php for( $i=0; $i<=(count($legend["name"])-1); $i++){ ?>
										<tr class="grp" id="grp-<?php echo $i+1; ?>">
											<?php if(empty($showgrps)==TRUE){ ?> <td class="grp">
											<nobr><input type="checkbox" class="grp-<?php echo $i+1; ?>"/><input type="checkbox" checked="checked" class="hidegrp-<?php echo $i+1; ?>" /></nobr>
											</td> <?php } ?>
											<td>
												<div id="square" style="background-color: #<?php echo $legend["color"][$i]; ?>; border:1px solid #<?php echo $legend["color"][$i]; ?>;"></div>
											</td>
											<td align="left" class="legname" id="leg-<?php echo $i; ?>" style="cursor: pointer">
												<?php echo $legend["name"][$i]; ?>
											</td>
										</tr> <?php } ?>
									</table>
								</div>
							</div>
							<button class="evalb">Toggle E-Values</button>
							<div class="emen" align="center"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="newseq">
				<div id="headw"><div id="newseq-hide" class="headtextmenu">hide tab</div><a href="../clans_web_viewer-help.pdf#page=9" target="_blank"><div id="newseqinfo" class="infopic"></div></a><div class="headtext">Add New Sequences:</div></div>
					<div class="hide">
						<div class="hmmerror" style="color:red"><br /><b>For the selected CLANS file no HMMs has been created. Therefore you cannot compare squences to this file. Please contact the uploader of this file or upload it again and wait for the HMM calculations to complete.</b><br /><br /></div>
						<dl class="checknohmms">
							<dt>
								Paste your FASTA Sequence here:
							</dt>
							<dd class="addseq">
								<textarea id="addfasta" rows="6" cols="40"></textarea><br>
								<button id="addfastab" disabled>Add</button>
							</dd>
							<dt>
								<br>You can also upload a file in FASTA format:
							</dt>
							<dd>
								<form class="sequp" action="../back/addfasta.php" method="post" target="upload_target_fasta" enctype="multipart/form-data">
									<p class="ftpform">
										<input type="file" name="file" class="file" />
										<input type="submit" name="upload" value="Upload" class="sequpb" disabled="disabled"/>
									</p>
									<iframe id="upload_target_fasta" name="upload_target_fasta"  style="width:0;height:0;border:0px solid #fff;"></iframe>
								</form>
							</dd>
						</dl>
					</div>
			</div>
			<div class="addedseqmain">
			<div class="addedseqtab" id="headw"><div id="addedseqtab-hide" class="headtextmenu">show tab</div><a href="../clans_web_viewer-help.pdf#page=9" target="_blank"><div id="addedseqinfo" class="infopic"></div></a><div class="headtext">Your added Sequences:</div></div>
				<div class="hide">
					<div class="scroll">
						<div class="pnamesadded">
							<button class="collapsall">Collapse all</button><button class="showall">Show all</button><button class="select">Select all</button><button class="deselect">Deselect all</button><button class="show3d">Show in 3D view</button><button class="rawoutput">Show raw output</button><br />
							<p class="hidetextseq">Nothing to show yet</p>
						</div>
					</div>
				</div>
			</div>
			<div class="names">
				<div id="headw"><div id="names-hide" class="headtextmenu">hide tab</div><a href="../clans_web_viewer-help.pdf#page=11" target="_blank"><div id="clansinfo" class="infopic"></div></a><div class="headtext">Entries from the CLANS file:</div></div>
				<div class="hide">
					<div class="scroll">
						<div class="pnames" align="left">
							<button class="deselect-seqs">Deselect all</button><br />
								<?php
								for( $i=0; $i<=(count($seqgrp["name"])-1); $i++){
									if($seqgrp["size"][$i]>0 && $seqgrp["hide"][$i]!=1){
										//get colors and devide by 255 to make it fit for html
										$colors=explode(";",$seqgrp["color"][$i]);
										$li++;
										//get the numbers from the grp array
										$numbers=explode(";",$seqgrp["numbers"][$i]);
									?> 	<div class="pointcheck" style="background: rgba(<?php echo $colors[0]; ?>, <?php echo $colors[1]; ?>, <?php echo $colors[2]; ?>, 0.3);"><form id="checkboxform" autocomplete = "off"> <?php
										echo "<span class='grpnr'>".$seqgrp["name"][$i]."</span><br />";
										for( $i1=0; $i1<=(count($numbers)-2); $i1++){
											$nr=$numbers[$i1];
											echo "<input type='checkbox' id='".$nr."'/>&nbsp;".$seqname[$nr]."<br />";
										} ?> </div></form> <?php
									}
								}
								?>
								<br><span class='grpnr'>Unassigned:<br /></span>
								<div class="pointcheck">
									<?php
										for( $i=0; $i<=(count($seqwogrp)-1); $i++){
											$posname = explode(" ",$seqwogrp[$i]);
											echo "<input type='checkbox' id='".$posname[0]."'/>&nbsp;".$seqname[$posname[0]]."<br />";
										}
									?>
								</div>
						</div>
					</div>
				</div>
			</div>
			<div id="show" class="info">
				<div id="headw" class="infolog"><div id="info-hide" class="headtextmenu">show tab</div><a href="../clans_web_viewer-help.pdf#page=12" target="_blank"><div id="loginfo" class="infopic"></div></a><div class="headtext">Info & Config:</div> </div>
				<div class="hide">
					<?php 
						if(empty($_POST["id"])==FALSE or empty($_GET["id"])==FALSE){
							$filename = $dirname.".txt";
							$fpath = $ddir.$filename;
							if(file_exists($fpath)==TRUE){
								?>
								<b>Configuration options:</b><br>
								Highlighted Points Scale:
									<div class="hpscale">
										<input class="number" value=1 step="0.1" type="number" min="0.1" max="10" disabled=disabled><button class="minus" disabled=disabled>-</button><button class="plus" disabled=disabled>+</button>
									</div>
										Group Scale Factor:
									<div class="gscale">
										<input class="number" value=1 step="0.01" type="number" min="0.01" max="2" ><button class="minus">-</button><button class="plus">+</button>
									</div>
										Backgroundcolor:<br>
									<div id="black"></div>
									<div id="swatch" class="ui-widget-content ui-corner-all"></div>
									<div>
										<br /><p class="advconfbut" style="cursor: pointer"><b>Advanced configuration [click to toggle]</b></p>
										<div class="advconf">
											<div class="eachgroup">
												Set the size of each group-sphere individually<br /><br />
												Restore from file:<br />
												<form class="phpupform" action="../back/fileIO.php?id=2" method="post" target="upload_target_data" enctype="multipart/form-data">
													<p id="uform">
														<input type="file" name="getfile" id="getfile" class="getfile" />
														<br />
														<input type="submit" name="getsubmit" value="Restore" class="readsize" disabled="disabled"/><br />
													</p>
													<iframe id="upload_target_data" name="upload_target_data" style="width:0;height:0;border:0px solid #fff;"></iframe>
												</form>
												Manual settings:<br />
												<?php for($i=0;$i<count($seqgrp["name"]);$i++){ ?>
												Group <?php echo $i+1; ?>: <?php echo $seqgrp["name"][$i]; ?><br />
												X-axis: <input id="<?php echo $i+1; ?>" class="sgx" value=1 step="0.01" type="number" min="0"><br />
												Y-axis: <input id="<?php echo $i+1; ?>" class="sgy" value=1 step="0.01" type="number" min="0"><br />
												Z-axis: <input id="<?php echo $i+1; ?>" class="sgz" value=1 step="0.01" type="number" min="0"><br />
												<?php } ?>
												<br /><button class="savesize">Save data</button>
											</div>									
										</div>	
									</div>
									<br>
								<?php
								//check if the sequences value matches the arraysize of name and pos. Show how many groups has been loaded
								//write it in a $
								echo "<div><b>This file contains ".$seqcount[1].$seqcount[0]."</b><br>";
								if($seqcount[1]==count($seqname) && $seqcount[1]==count($seqpos)){
								echo "Integritycheck: <br>".count($seqname)."/".$seqcount[1]." names have been processed<br>"; 
								echo count($seqpos)."/".$seqcount[1]." positions have been processed<br>"; 
								echo count($seqgrp["name"])." groups have been processed<br>"; 
								}
								else{
									echo "<h1><span style='color: red;'>An error has occured! The illustration may not be correct or your file may be corrupted!</span></h1><br><br>";
								}
								echo ($pgrp-1)." groups are visible<br>";
								echo"completed</b><br><br></div>";
								?> <b>Alignments:</b><ul> <?php
								for($i=0;$i<count($seqgrp["name"]);$i++){
									$repl = str_replace("/","",$seqgrp["name"][$i]);
									echo "<li><a href='$ddir$dirname/$repl.aln' target='_blank'>".$seqgrp["name"][$i]."</a></li>";
								}
								echo "</ul>";
							}
							else{ echo "<h1><span style='color: red;'><b>This is not a valid ID! Please visit the <a href='index.php'>../index.php</a></b></span></h1>"; }
						}
						else{ echo "<h1><span style='color: red;'><b>You should not be here! Please visit the <a href='../index.php'>index.php</a></b></span></h1>"; } ?>
				</div>
			</div>
			<div id="footer">
			<span class="fooleft">Colorchange: <span id="colchange" class="colred" data-file="main_red">red</span><span id="colchange" class="colgreen" data-file="main_green">green</span></span><span class="fooright">About</span>
			</div>
		</div>
	<script>
		(function(){
			//check for HMMs
			var hmmavail = <?php echo $hmmavail; ?>;
			if(hmmavail==0){ $("dl.checknohmms").hide(); }
			else { $("div.hmmerror").hide(); }
			//bugfix for local saving
			if($('div#container').children().eq(2).length!==0){ $('div#container').children().eq(2).remove(); }
			$('.errormessage').hide();
			var container, renderer, camera, scene, color, controls;
			//set the camera position based on the center of all points and zoom out if file is big
			distance = new THREE.Vector3();
			distance.x = parseFloat(<?php echo $centerpoint[0]; ?>);
			distance.y = parseFloat(<?php echo $centerpoint[1]; ?>);
			if(<?php echo count($seqname); ?> <=5000){
				distance.z = parseFloat(<?php echo $centerpoint[2]; ?>)+25;
			}
			else if(<?php echo count($seqname); ?> >=5000 && <?php echo count($seqname); ?> <=10000){
				distance.z = parseFloat(<?php echo $centerpoint[2]; ?>)+60;
			}
			else if(<?php echo count($seqname); ?> >=10000 && <?php echo count($seqname); ?> <=20000){
				distance.z = parseFloat(<?php echo $centerpoint[2]; ?>)+90;
			}
			else{ distance.z = parseFloat(<?php echo $centerpoint[2]; ?>)+130; }
			DEFDIST = distance;
			init(true);
			initparticlesys(true);
			animate();
			//init some variables (php -> javascript)
			<?php print "if(typeof(pgrp)=='undefined'){pgrp = ".$pgrp."-1;}"; ?>
			<?php print "if(typeof(seqpos)=='undefined'){seqpos = ".json_encode($seqpos)."; hpscale=1; point = {};}"; ?>
			<?php print "if(typeof(grpname)=='undefined'){grpname = ".json_encode($legend["name"])."; for(i=0;i<grpname.length;i++){grpname[i]=grpname[i].replace('/','');} meanx = ".json_encode($mean["x"])."; meany = ".json_encode($mean["y"])."; meanz = ".json_encode($mean["z"])."; }"; ?>
			<?php print "ocolor1 = ".json_encode($legend["ocolor1"])."; ocolor2 = ".json_encode($legend["ocolor2"])."; ocolor3 = ".json_encode($legend["ocolor3"])."; grpcolor = ".json_encode($legend["color"]).";" ?>
			if(typeof(countleg)=='undefined'){ countleg = "<?php echo count($legend["name"])-1; ?>"; }
			if(typeof(dirnameseq)=='undefined'){ dirnameseq = "<?php echo getcwd() ."/".$ddir.$dirname; ?>"; }
			if(typeof(dirname)=='undefined'){ dirname = "<?php echo $dirname; ?>"; }
			<?php if(isset($rawfile)){ echo "if(typeof(rawfile)=='undefined'){ rawfile='".$rawfile."'; }"; } ?>
			<?php //add sequences if ?hs is specified
				if(empty($rawfile)==false){
					?> geths = true; <?php
				}
			?>
			<?php //add sequences from the index textfield and change url
				if(empty($addedseq)==false){
					$second=false;
					//if no name is specified add a dummy name:
					$testseqname = str_split($addedseq);
					if($testseqname[0]!=" " && $testseqname[0]!=">"){
						$addedseq="> dummy name\n".$addedseq;
					}
					elseif($testseqname[0]!=" "){
						for($i=0;$i<10;$i++){
							if($testseqname[0]!=" " && $testseqname[0]!=">"){
								$addedseq="> dummy name\n".$addedseq;
							}
						}
					}
					//check and correct sequences
					$addedseqpost="";
					$addedseqpre = explode("\n", $addedseq);
					for($i=0;$i<count($addedseqpre);$i++){
						$testseq = str_split($addedseqpre[$i]);
						if($testseq[0]==">"){
							if($second == true){
								$addedseqpre[$i] = preg_replace('/[^(\x20-\x7F)]*/','', $addedseqpre[$i]);
								$addedseqpost .= "\n".$addedseqpre[$i]."\n";
							}
							else{
								$addedseqpre[$i] = preg_replace('/[^(\x20-\x7F)]*/','', $addedseqpre[$i]);
								$addedseqpost .= $addedseqpre[$i]."\n";
								$second = true;
							}
						}
						else{
							if($addedseqpre[$i]!=" "){
								$addedseqpre[$i] = preg_replace('/[^(\x20-\x7F)]*/','', $addedseqpre[$i]);
								$addedseqpost .= $addedseqpre[$i];
							}
						}
					}
					?> seqfield = <?php echo json_encode($addedseqpost); ?>; 
					indexseq = true;
			<?php }
			//change URL if just 3D view is clicked
			if(empty($addedseq)!=false && empty($rawfile)!=false && empty($_GET["id"])!=false){?> 
				just3d = true; 
			<?php }
			if(empty($_GET["id"])==false && empty($addedseq)!=false && empty($rawfile)!=false){ ?>
				justid = true;
			<?php } ?>
		})();
	</script>
	</body>
</html>