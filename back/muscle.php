<?php
// muscle.php
// 
// CLANS Web Viewer, an web application for proteinclassification.
// Copyright (C) 2012 Jens Rauch
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program. If not, see http://www.gnu.org/licenses.
include("../config.php");
if(isset($_POST["a"])){$ufile = substr($_POST["a"],0,-4);}
$check = $_POST["b"];
if(isset($_POST["c"])){$pid = $_POST["c"];}
$fpath = $_POST["d"];
function getvals(){
	$fpath = $_POST["d"];
	$file=fopen($fpath, "r") or exit("Unable to open file!");
	if($file){
		//get first line (Sequences)
		$line=fgets($file);
		$seqcount=explode("=",$line,2);
		$i=0;
		$i9=0;
		$startseq=0;
		global $seqname;
		$seqgrp = array("name"=>array(),"type"=>array(),"size"=>array(),"hide"=>array(),"color"=>array(),"numbers"=>array());
		while(!feof($file)){		
			$line=fgets($file);
			
			//check if the line begins with ">" -> name
			//check if the line is in the <pos> tag
			if(strcmp(trim($line),"</seq>")==0){
				$startseq=0;
			}
			if($startseq==1){
				$checkline=str_split($line);
				if(strcmp($checkline[0],">")==0){
					$seqname[$i9]=$line;
				}
				else{
					//check if line conains more than just the sequence
					$secu = explode(" ", $line);
					if(count($secu)>2){$result = "<br><b><p style='color: red'>Error: Your sequence line contains more than just the sequence! Please check it.</p></b><br>"; exit;}
					else{$seq[$i9]=str_replace(" ","",$line); $i9++;} 
				}
			}
			if(strcmp(trim($line),"<seq>")==0){
				$startseq=1;
			}	
		}
	}
}
function run_in_background($command){
	$fpath = $_POST["d"];
	$dir = substr($fpath,0,-4);
	$tempfile = $dir.'/muscletemp.temp';
	$PID = system( 'nice '.$command.' >'.$tempfile.' 2>&1 & echo $!' );
	return($PID);
}
function is_process_running($PID){
	exec("ps $PID", $ProcessState);
	echo count($ProcessState) >= 2;

}
function kill_process($PID){
	exec("kill $PID");
}
//check 0: execute alignment tool
if($check==0){
	$tmp=explode("/",$ufile);
	$exists=array_pop($tmp);
	$copypath="";
	array_pop($tmp);
	array_pop($tmp);
	foreach($tmp as $value){
		$copypath=$copypath."/".$value;
	}
	if(file_exists($copypath."/ftpuploads/".$exists.".aln")){
		$PID = system( 'mv '.$copypath.'/ftpuploads/'.$exists.'.aln '.$ufile.'.aln 2>&1 & echo $!' );
		echo("<p>This alignment has been provided. Skipping...</p>");
		return($PID);
	}
	else{
		if($aligntool=="muscle"){
			$out = array();
			exec("wc -l ".$ufile.".txt 2>&1", $out);
			$seqnuming = explode(" ", $out[0]);
			if(($seqnuming[0]/2)>500){ $command = "/usr/bin/muscle -maxiters 2 -in ".$ufile.".txt -out ".$ufile.".aln"; }
			else{ $command = "/usr/bin/muscle -in ".$ufile.".txt -out ".$ufile.".aln"; }
		}
		elseif($aligntool=="clustalw"){
			if(file_exists("/usr/bin/clustalw2")==true){ $command = "/usr/bin/clustalw2 -INFILE=".$ufile.".txt -OUTFILE=".$ufile.".aln -OUTPUT=FASTA"; }
			else{ $command = "/usr/bin/clustalw -INFILE=".$ufile.".txt -OUTFILE=".$ufile.".aln -OUTPUT=FASTA"; }
		}
		elseif($aligntool=="clustalo"){
			if($cpucores=="default"){ $command = "/usr/bin/clustalo --in ".$ufile.".txt --out ".$ufile.".aln"; }
			else{ $command = "/usr/bin/clustalo --in ".$ufile.".txt --out ".$ufile.".aln --threads=".$cpucores; }
		}
		run_in_background($command);
	}
	unset($tmp);
	unset($exists);
	unset($copypath);
}
//check 1: stop alignment tool if script was stoped
elseif($check==1){
	kill_process($pid);
}
//check 2: chmod alignment tool output && replace sequencenumbers with names
elseif($check==2){
	getvals();
	chmod($ufile.".aln", 0777);
	if (!$handle2 = fopen($ufile.".aln", "r")){
		$result = "<br><b>cannot open file ".$ufile.".aln"."</b><br>";
		exit;
	}
	else{
		$donotwrite=0;
		$newfile="";
		while(!feof($handle2)){
			$line=fgets($handle2);
			$checkline=str_split($line);
			if(strcmp($checkline[0],">")==0){
				$newline = explode(">",$line);
				$newfile .= $seqname[trim($newline[1])];
				$donotwrite++;
			}
			else{$newfile .= $line;}
		}
		fclose($handle2);
		if (!$handle2 = fopen($ufile.".aln", "w")){
		$result = "<br><b>cannot open file ".$ufile.".aln"."</b><br>";
		exit;
		}
		if(!fwrite($handle2, $newfile)){
		$result = "<br><b>cannot write to file</b>";
		exit;
		}
	}
	if(empty($result)==FALSE){ echo $result; }
}
//check progress
elseif($check==3){
	$fpath = $_POST["d"];
	$dir = substr($fpath,0,-4);
	$tempfile = $dir.'/muscletemp.temp';
	$progress = exec('strings '.$tempfile.'| tail -n 1');
	$preout = preg_replace ('#\s+#' , ' ' , $progress);
	$output = explode(" ",$preout);
	$output1 = explode("%",$preout);
	if(count($output)>=4 ){
		echo "<p>Time passed: ".$output[0]."<br />".
			"Size: ".$output[1]." ".$output[2]."<br />".
			"Iteration: ".$output[4]."<br />".
			"\"".$output1[2]."\""." to ".$output[5]." completed </p><br>";
	}
	else{ echo "<p style='color: red'>wait a moment and try again</p>"; }
}
elseif($check==4){
	is_process_running($pid);
}
elseif($check==5){
	$fpath = $_POST["d"];
	$tempfile = $fpath.'/muscletemp.temp';
	$file=fopen($tempfile, "r") or exit("Unable to open file!");
	if($file){
		while(!feof($file)){
			$line=fgets($file);
			$warning = explode(" ", $line);
			if($warning[0]=="***"){ echo "<p style='color: red'>".$line."/<p>"; }
		}
	}
}
?>