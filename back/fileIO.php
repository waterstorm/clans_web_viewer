<?php
if(isset($_POST["id"])){$check = $_POST["id"];}
if(isset($_GET["id"])){$check = $_GET["id"];}
$NAME="data.txt";
$id=0;
while(file_exists("../saved_files/".$NAME)==TRUE){ $id++; $NAME = $id.".data.txt";}
//case one: save data to text file
if($check==1){
	$HANDLE = fopen("../saved_files/".$NAME, 'w') or die ('CANT OPEN FILE');
	fwrite($HANDLE,$_POST["data"]);
	fclose($HANDLE);
	echo $NAME;
}
//case two: load data from text file
if($check==2){
	if ($_FILES["getfile"]["error"] > 0)
	   {
	   	echo "Error: " . $_FILES["getfile"]["error"] . "<br />";
		exit();
	   }
	//Check for filename errors and correct them
	$SafeFile = $_FILES["getfile"]["name"];
	$uploaddir = "../orig/";
	//check for php file, not allowed for security reasons
	$tmp=explode(".",$SafeFile);
	$isphp=array_pop($tmp);
	$phpending=array("php","php3","php4","php5");
	if(in_array($isphp, $phpending)){
		unlink($_FILES["getfile"]["tmp_name"]);
		exit("PHP file upload is not allowed due to security reasons!");
	}
	unset($tmp);
	//create a temporary name, in case some uploads a file with the same name simultaneous
	$path = tempnam($uploaddir, "ORG");
	move_uploaded_file($_FILES['getfile']['tmp_name'], $path);
	$HANDLE = fopen($path,'r') or die ('CANT OPEN FILE');;
	$DATA = fread($HANDLE,filesize($path));
	fclose($HANDLE);
	$getdata = explode("\n", $DATA);
	array_pop($getdata);
	?> <script> <?php echo "window.top.window.getdata = ".json_encode($getdata).";"; ?> </script> <?php
	unlink($path);
}
//case three: delete text file from server
if($check==3){
	unlink("../saved_files/".$_POST["data"]);
}
?>
