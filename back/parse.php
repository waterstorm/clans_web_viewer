<?php
// parse.php
// 
// CLANS Web Viewer, an web application for proteinclassification.
// Copyright (C) 2012 Jens Rauch
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program. If not, see http://www.gnu.org/licenses.
$uform = $_GET["uform"];
if($uform=="direct"){
	header('Content-Type: text/html; charset=utf-8');
	header('X-Content-Type-Options: nosniff');
	header('X-XSS-Protection: 1; mode=block');
	set_time_limit(0);
}
echo '<script language="javascript" type="text/javascript">window.top.window.deact();</script>';
if($uform=="direct"){
	if ($_FILES["file"]["error"] > 0)
	   {
	   	echo "Error: " . $_FILES["file"]["error"] . "<br />";
		exit();
	   }
	//Check for filename errors and correct them
	$SafeFile = $_FILES["file"]["name"];
	$uploaddir = "../orig/";
}								
else{
	$SafeFile = $_POST["passover"];
	$uploaddir = "../ftpuploads/";
}
//Check for filename errors and correct them
$SafeFile = str_replace("#", "No.", $SafeFile); 
$SafeFile = str_replace('$', "Dollar", $SafeFile); 
$SafeFile = str_replace("%", "Percent", $SafeFile); 
$SafeFile = str_replace("^", "", $SafeFile); 
$SafeFile = str_replace("&", "and", $SafeFile); 
$SafeFile = str_replace("*", "", $SafeFile); 
$SafeFile = str_replace("?", "", $SafeFile);
//check for php file, not allowed for security reasons
$tmp=explode(".",$SafeFile);
$isphp=array_pop($tmp);
$phpending=array("php","php3","php4","php5");
if(in_array($isphp, $phpending)){
	unlink($_POST["passover"]);
	echo '<script language="javascript" type="text/javascript">window.top.window.uploadcomplete("ERROR!","<p>You have selected a .php file. This is due to security reasons not possible, please choose a valid CLANS file.<br>You can get back by clicking on the CLANS Web Viewer name in the top left corner of the page.</p>","100");</script>';
	exit("PHP file upload is not allowed due to security reasons!");
}
unset($tmp);
$id=0;
$SFile = $id.".".$SafeFile;
if($uform=="direct"){
	//create a temporary name, in case some uploads a file with the same name simultaneous
	$path = tempnam($uploaddir, "ORG");
	while(file_exists("../saved_files/".$SFile)==TRUE){ $id++; $SFile = $id.".".$SafeFile;}	
	if(empty($SFile)==FALSE){ //AS LONG AS A FILE WAS SELECTED... 
		if(move_uploaded_file($_FILES['file']['tmp_name'], $path)){ //IF IT HAS BEEN COPIED... 
		//GET FILE NAME 
		$theFileName = $_FILES['file']['name']; 
		//GET FILE SIZE 
		$theFileSize = $_FILES['file']['size']; 
		if ($theFileSize>999999999){ //IF GREATER THAN 999MB, DISPLAY AS GB 
			$theDiv = $theFileSize / 1000000000; 
			$theFileSize = round($theDiv, 1)." GB";
		}
		elseif ($theFileSize>999999){ //IF GREATER THAN 999KB, DISPLAY AS MB 
			$theDiv = $theFileSize / 1000000; 
			$theFileSize = round($theDiv, 1)." MB";
		}
		else { //OTHERWISE DISPLAY AS KB 
			$theDiv = $theFileSize / 1000; 
			$theFileSize = round($theDiv, 1)." KB";
		} 
		
		$result='<br>File Name: '.$theFileName.'<br>File Size: '.$theFileSize.'<br> Upload Successful!<br>';
		
		} else { 
			//PRINT AN ERROR IF THE FILE COULD NOT BE COPIED 
			$result = '<br><b>Error! File could not be uploaded.<br>Filename was '.$_FILES['file']['name'].'<br>Path was '.$path.'</b>';
		} 
	}
}
else{
	$path = $uploaddir.$SafeFile;
	while(file_exists("../saved_files/".$SFile)==TRUE){ $id++; $SFile = $id.".".$SafeFile;}
}

echo '<script language="javascript" type="text/javascript">window.top.window.uploadcomplete("Processing File...","'.$result.'","5");</script>';
$filename = $SFile;
if (strrpos($filename, '.') != 1) {
	$filename = substr($filename, 0, strrpos($filename, '.'));
	$filename .= ".txt";
}
else {
	$filename .= ".txt";
}
$ddir = "../saved_files/";
$fpath = $ddir.$filename;
$file=fopen($path,"r") or exit("Unable to open file!");

if($file){
	//remove <hsp> part of the file (not needed and VERY large) and save it as a temp file, remove original
	if(file_exists($fpath)==FALSE){
		$line=fgets($file);
		$seqcount=explode("=",$line,2);
		if($seqcount[0]!="sequences"){
			unlink($_POST["passover"]);
			echo '<script language="javascript" type="text/javascript">window.top.window.uploadcomplete("Error no valid CLANS file!","<p>The opened file is no valid CLANS file, please return to the upload page and select a valid CLANS file.<br>You can get back by clicking on the CLANS Web Viewer name in the top left corner of the page.</p>","100");</script>';
			exit("The uploaded file is no valid CLANS file!");
		}
		$buffer= $line;
		$i=0;
	    while(!feof($file)) {
	    	$line=fgets($file);
			$buffer.= $line;
	    	if(strcmp(trim($line),"</pos>")!=0){
	    		if($i==0){
	    			$result = "<br>Processing file...<br>";
					$result .= "ID: ".$id."<br>";
					$i++;
				}
				else {
					$i++;
				}				
			}
			else{
				if (!$handle = fopen($fpath, "w"))
				{
					$result = "<br><b>cannot open file</b>";
					exit;
				}
				if(!fwrite($handle, $buffer))
				{
					$result = "<br><b>cannot write to file</b>";
					exit;
				}
				fclose($handle);
				$result .= "Done!<br>";
				break;
			}
		}
	}
}

fclose($file);
$result .= "calculating a PCA please wait...<br>";
echo '<script language="javascript" type="text/javascript">window.top.window.uploadcomplete("Calculating PCA...","'.$result.'","10");</script>';
include("pca.php");
//get the sequence positions
$file=fopen($fpath, "r") or exit("Unable to open file!");

if($file){
	//get first line (Sequences)
	$line=fgets($file);
	$seqcount=explode("=",$line,2);
	$i=0;
	$i2=0;
	$i3=0;
	$i9=0;
	$startpos=0;
	$startseq=0;
	$startgrp=0;
	$seqgrp = array("name"=>array(),"type"=>array(),"size"=>array(),"hide"=>array(),"color"=>array(),"numbers"=>array());
	while(!feof($file)){		
		$line=fgets($file);
		
		//check if the line begins with ">" -> name
		//check if the line is in the <pos> tag
		if(strcmp(trim($line),"</seq>")==0){
			$startseq=0;
		}
		if($startseq==1){
			$checkline=str_split($line);
			if(strcmp($checkline[0],">")==0){
				$seqname[$i9]=$line;
			}
			else{
				//check if line conains more than just the sequence
				$secu = explode(" ", $line);
				if(count($secu)>2){$result = "<br><b>Error: Your sequence line contains more than just the sequence! Please check it.</b><br>"; exit;}
				else{$seq[$i9]=str_replace(" ","",$line); $i9++;} 
			}
			
		}
		if(strcmp(trim($line),"<seq>")==0){
			$startseq=1;
		}
		
		//check if the line is in the </seqgroups> tag
		if(strcmp(trim($line),"</seqgroups>")==0){
			$startgrp=0;
		}	
		
		//add the lines to a multidimensional array
		if($startgrp==1){
			$checkline=explode("=", $line);			
			if(strcmp(trim($checkline[0]),"name")==0){
				$seqgrp["name"][$i3] = $checkline[1];
			}
			elseif(strcmp(trim($checkline[0]),"type")==0){
				$seqgrp["type"][$i3] = $checkline[1];
			}
			elseif(strcmp(trim($checkline[0]),"size")==0){
				$seqgrp["size"][$i3] = $checkline[1];
			}
			elseif(strcmp(trim($checkline[0]),"hide")==0){
				$seqgrp["hide"][$i3] = $checkline[1];
			}
			elseif(strcmp(trim($checkline[0]),"color")==0){
				$seqgrp["color"][$i3] = $checkline[1];
			}
			elseif(strcmp(trim($checkline[0]),"numbers")==0){
				$seqgrp["numbers"][$i3] = $checkline[1];
			$i3++;
			}			
		}
		if(strcmp(trim($line),"<seqgroups>")==0){
			$startgrp=1;
		}
		
		//check if the line is in the <pos> tag
		if(strcmp(trim($line),"</pos>")==0){
			$startpos=0;
		}	
		if($startpos==1){
			//add every line while in the <pos> tag to the $seqpos array
			$seqpos[$i2]=$line;
			$i2++;
		}
		if(strcmp(trim($line),"<pos>")==0){
			$startpos=1;
		}					
	}
}
fclose($file);

if (!$handle = fopen($fpath, "a")){
	$result = "<br><b>cannot open file ".$fpath."</b>";
	exit;
}
else{
	//write mean, eigenvalue and eigenvectors in the temp file
	if(count($seqgrp["numbers"])>0)
	{
		$buffermean="<mean>"."\r\n";
		$bufferval="<eval>"."\r\n";
		$buffervec="<evec>"."\r\n";
		//just use the ones >size=0 && !=hide=1
		for( $i=0; $i<=(count($seqgrp["name"])-1); $i++){
			if($seqgrp["size"][$i]>0 && $seqgrp["hide"][$i]!=1){
				//get the numbers from the grp array
				$numbers=explode(";",$seqgrp["numbers"][$i]);
				//get coordinates for every point	
				for( $i1=0; $i1<=(count($numbers)-2); $i1++){
					$nr=$numbers[$i1];
					$coords=explode(" ",$seqpos[$nr]);
					$vec["x"][$i1]=$coords[1];
					$vec["y"][$i1]=$coords[2];
					$vec["z"][$i1]=$coords[3];
				}
				if((count($numbers)-1)>1){
					$meanx=0;
					$meany=0;
					$meanz=0;
					$eigenvecl = array();
					if(empty($eigenvecl)!=TRUE){unset($eigenvecl);}
					pca($vec);
					for($i2=-1;$i2<=count($eigenval)-1;$i2++){
						if($i2==-1){ $bufferval.="name=".$seqgrp["name"][$i]; }
						elseif($i2==0){ $bufferval.="x=".$eigenval[$i2][0]."\r\n"; }
						elseif($i2==1){ $bufferval.="y=".$eigenval[$i2][0]."\r\n"; }
						elseif($i2==2){ $bufferval.="z=".$eigenval[$i2][0]."\r\n"; }
					}
					for($i3=-1;$i3<=2;$i3++){
						if($i3==-1){$buffervec.="name=".$seqgrp["name"][$i];}
						elseif($i3==0){ $buffervec.="x=".$eigenvecl["0"][$i3]["0"].",".$eigenvecl["1"][$i3]["0"].",".$eigenvecl["2"][$i3]["0"]."\r\n"; }
						elseif($i3==1){ $buffervec.="y=".$eigenvecl["0"][$i3]["0"].",".$eigenvecl["1"][$i3]["0"].",".$eigenvecl["2"][$i3]["0"]."\r\n"; }
						elseif($i3==2){ $buffervec.="z=".$eigenvecl["0"][$i3]["0"].",".$eigenvecl["1"][$i3]["0"].",".$eigenvecl["2"][$i3]["0"]."\r\n"; }
					}
					for($i2=-1;$i2<=2;$i2++){
						if($i2==-1){ $buffermean.="name=".$seqgrp["name"][$i]; }
						elseif($i2==0){ $buffermean.="x=".$meanx."\r\n"; }
						elseif($i2==1){ $buffermean.="y=".$meany."\r\n"; }
						elseif($i2==2){ $buffermean.="z=".$meanz."\r\n"; }
					}
					unset($vec);
				}
				else{
					//check if the group contains only one point
					$buffermean.="name=".$seqgrp["name"][$i];
					$buffermean.="onepoint"."\r\n";
					$buffervec.="name=".$seqgrp["name"][$i];
					$buffervec.="onepoint"."\r\n";
					$bufferval.="name=".$seqgrp["name"][$i];
					$bufferval.="onepoint"."\r\n";
					
				}
				
			}
		}
	$buffermean.="</mean>"."\r\n";
	$bufferval.="</eval>"."\r\n";
	$buffervec.="</evec>"."\r\n";
	//write to file
	if(!fwrite($handle, $buffermean)){
					$result = "<br><b>cannot write to file</b>";
					exit;
	}
	if(!fwrite($handle, $bufferval)){
					$result = "<br><b>cannot write to file</b>";
					exit;
	}
	if(!fwrite($handle, $buffervec)){
					$result = "<br><b>cannot write to file</b>";
					exit;
	}
	}
fclose($handle);
}
$result = "<br>PCA done and written to temp file.<br>";
if (!unlink($path)){ $result = "<br><b>Error deleting ".$path."</b>"; }
else{ $result .= "Deleted original file from server<br><br>";}
$result .= "Creating group files for HMM<br>";
echo '<script language="javascript" type="text/javascript">window.top.window.uploadcomplete("Creating File Structure...","'.$result.'","15");</script>';
//get the directory and create a new folder
$dirname = substr($filename, 0, strrpos($filename, '.'));
$thisdir = getcwd() ."/../saved_files/".$dirname;
$hm=0;
for($i=0;$i<=(count($seqgrp["name"])-1);$i++){
	if($seqgrp["size"][$i]>0 && $seqgrp["hide"][$i]!=1){
		$hm++;
	}
}
if(mkdir($thisdir , 0777)){ chmod($thisdir, 0777); $result = "<br>Directory has been created successfully<br>"; }
else{ $result = "<br><b>Failed to create directory</b><br>";}
$i3=0;
for($i=0;$i<=(count($seqgrp["name"])-1);$i++){
	if($seqgrp["size"][$i]>0 && $seqgrp["hide"][$i]!=1){
		$chars = array(";", "/");
		$fixedgrp = str_replace($chars, "", $seqgrp["name"][$i]);
		$ufile = $thisdir."/".trim($fixedgrp).".txt";
		if($i3<=$hm){ $usedgrps[$i3]=$fixedgrp; }
		$i3++;
		if (!$handle = fopen($ufile, "w")){
			$result = "<br><b>cannot open file ".$ufile."</b><br>";
			exit;
		}
		else{
			chmod($ufile, 0777);
			//get the numbers from the grp array
			$numbers=explode(";",$seqgrp["numbers"][$i]);
			//get names and seqence for every point
			$buffer = "";
			for( $i2=0; $i2<=(count($numbers)-2); $i2++){
			$nr=$numbers[$i2];
			$buffer.=">".$nr."\n";
			$buffer.=$seq[$nr];;
			}
		}
		if(!fwrite($handle, $buffer)){
		$result = "<br><b>cannot write to file</b>";
		exit;
		}
		if (!$handle2 = fopen($ufile, "r")){
			$result = "<br><b>cannot open file ".$ufile."</b><br>";
			exit;
		}
		else{
			$donotwrite=0;
			while(!feof($handle2)){
				$line=fgets($handle2);
				$checkline=str_split($line);
				if(strcmp($checkline[0],">")==0){
					$donotwrite++;
				}
			}
		fclose($handle2);
		$hmm[$i3]=$donotwrite;
		}
	}
	if($i==(count($seqgrp["name"])-1)){
		shell_exec("cat $thisdir/*.txt > $thisdir/background.txt");
	}
}
?>
<script>
window.top.window.hm = <?php echo $hm; ?>;
window.top.window.dir = "<?php echo $thisdir; ?>";
window.top.window.mainfile = "<?php echo $fpath; ?>";
window.top.window.saveid = "<?php echo $dirname; ?>";
window.top.window.numberofgrps = "<?php echo count($seqgrp["name"]); ?>";
<?php echo "window.top.window.usedgrps = ".json_encode($usedgrps).";"; ?>
<?php echo "window.top.window.hmmwrite = ".json_encode($hmm).";"; ?>
</script>
<?php
echo '<script language="javascript" type="text/javascript">window.top.window.finish();</script>';
?>
