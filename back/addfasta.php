<?php
// addfasta.php
// 
// CLANS Web Viewer, an web application for proteinclassification.
// Copyright (C) 2012 Jens Rauch
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program. If not, see http://www.gnu.org/licenses.
if ($_FILES["file"]["error"] > 0)
   {
   	echo "Error: " . $_FILES["file"]["error"] . "<br />";
   }
else{
	//Check for filename errors and correct them
	$SafeFile = $_FILES["file"]["name"]; 
	$SafeFile = str_replace("#", "No.", $SafeFile); 
	$SafeFile = str_replace('$', "Dollar", $SafeFile); 
	$SafeFile = str_replace("%", "Percent", $SafeFile); 
	$SafeFile = str_replace("^", "", $SafeFile); 
	$SafeFile = str_replace("&", "and", $SafeFile); 
	$SafeFile = str_replace("*", "", $SafeFile); 
	$SafeFile = str_replace("?", "", $SafeFile);
	$uploaddir = "../orig/";
	//Add a random number to the file name, in case some uploads a file with the same name simultaneous
	$file = tempnam($uploaddir, "FOO");
	if(move_uploaded_file($_FILES['file']['tmp_name'], $file)){ 
		$result='<p>Upload Successful!</p>';
		//read file and put content to variable
		$second=false;
		$addedseq = file_get_contents($file);
		//if no name is specified add a dummy name:
		$testseqname = str_split($addedseq);
		if($testseqname[0]!=" " && $testseqname[0]!=">"){
			$addedseq="> dummy name\n".$addedseq;
		}
		elseif($testseqname[0]!=" "){
			for($i=0;$i<10;$i++){
				if($testseqname[0]!=" " && $testseqname[0]!=">"){
					$addedseq="> dummy name\n".$addedseq;
				}
			}
		}
		$addedseqpost="";
		$addedseqpre = explode("\n", $addedseq);
		for($i=0;$i<count($addedseqpre);$i++){
			$testseq = str_split($addedseqpre[$i]);
			if($testseq[0]==">"){
				if($second == true){
					$addedseqpre[$i] = preg_replace('/[^(\x20-\x7F)]*/','', $addedseqpre[$i]);
					$addedseqpost .= "\n".$addedseqpre[$i]."\n";
				}
				else{
					$addedseqpre[$i] = preg_replace('/[^(\x20-\x7F)]*/','', $addedseqpre[$i]);
					$addedseqpost .= $addedseqpre[$i]."\n";
					$second = true;
				}
			}
			else{
				if($addedseqpre[$i]!=" "){
					$addedseqpre[$i] = preg_replace('/[^(\x20-\x7F)]*/','', $addedseqpre[$i]);
					$addedseqpost .= $addedseqpre[$i];
				}
			}
		}
		//call addseq function
		?> <script language="javascript" type="text/javascript">
				seqfield = <?php echo json_encode($addedseqpost); ?>;
				window.top.finish(seqfield);
			</script> <?php
	} else { 
		//PRINT AN ERROR IF THE FILE COULD NOT BE COPIED 
		$result = '<br><b>Error! File could not be uploaded.<br>Filename was '.$_FILES['file']['name'].'<br>Path was '.$path.'</b>';
	}
}
?>