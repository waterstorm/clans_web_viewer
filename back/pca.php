<?php
// pca.php
// 
// CLANS Web Viewer, an web application for proteinclassification.
// Copyright (C) 2012 Jens Rauch
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program. If not, see http://www.gnu.org/licenses.
function covar($arraya, &$arrayb){
	//arraya = input | arrayb = output
	$arrayb["x"][0] = stats_covariance($arraya["x"], $arraya["x"]);
	$arrayb["y"][0] = stats_covariance($arraya["y"], $arraya["x"]);
	$arrayb["z"][0] = stats_covariance($arraya["z"], $arraya["x"]);
	$arrayb["x"][1] = stats_covariance($arraya["x"], $arraya["y"]);
	$arrayb["y"][1] = stats_covariance($arraya["y"], $arraya["y"]);
	$arrayb["z"][1] = stats_covariance($arraya["z"], $arraya["y"]);
	$arrayb["x"][2] = stats_covariance($arraya["x"], $arraya["z"]);
	$arrayb["y"][2] = stats_covariance($arraya["y"], $arraya["z"]);
	$arrayb["z"][2] = stats_covariance($arraya["z"], $arraya["z"]);
}
function vecMean($arraya, &$arrayb){
	//arraya = input | arrayb = output
	$GLOBALS["meanx"]=array_sum($arraya["x"])/count($arraya["x"]);
	$GLOBALS["meany"]=array_sum($arraya["y"])/count($arraya["y"]);
	$GLOBALS["meanz"]=array_sum($arraya["z"])/count($arraya["z"]);
	
	for($i=0;$i<=(count($arraya["x"])-1);$i++){
		$arrayb["x"][$i] = $arraya["x"][$i] - $GLOBALS["meanx"];
		$arrayb["y"][$i] = $arraya["y"][$i] - $GLOBALS["meany"];
		$arrayb["z"][$i] = $arraya["z"][$i] - $GLOBALS["meanz"];
	}
}
function pca($arraya){
	global $vecm;
	if(empty($vecm)!=TRUE){unset($vecm);}
	$vecm = array("x"=>array(),"y"=>array(),"z"=>array());
	vecMean($arraya, $vecm);
	global $covar;
	if(empty($covar)!=TRUE){unset($covar);}
	$covar = array("x"=>array(),"y"=>array(),"z"=>array());
	coVar($vecm, $covar);
	global $eigenval;
	$eigenval = Lapack::eigenValues($covar,$GLOBALS["eigenvecl"]);
}
function f_remove_odd_characters($string){
	$string = str_replace("\n","[NEWLINE]",$string);
	$string=htmlentities($string);
	$string=preg_replace('/[^(\x20-\x7F)]*/','',$string);
	$string=html_entity_decode($string);
	$string = str_replace("[NEWLINE]","\n",$string);
	return $string;
}
?>