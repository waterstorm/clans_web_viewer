<?php
// parsearray.php
// 
// CLANS Web Viewer, an web application for proteinclassification.
// Copyright (C) 2012 Jens Rauch
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program. If not, see http://www.gnu.org/licenses.

function in_array_r($needle, $haystack, $strict = true) {
    foreach ($haystack as $item) {
        if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
            return true;
        }
    }

    return false;
}
$file=fopen($fpath, "r") or exit("Unable to open file!");

if($file){	
	//get first line (Sequences)
	$line=fgets($file);
	$seqcount=explode("=",$line,2);
	$i=0;
	$i2=0;
	$i3=0;
	$i4=0;
	$i5=0;
	$i6=0;
	$i7=0;
	$i8=0;
	$i9=0;
	$startpos=0;
	$startgrp=0;
	$startmean=0;
	$starteval=0;
	$startevec=0;
	$startseq=0;
	$numbers=array();
	$seqwogrp = array();
	$seqgrp = array("name"=>array(),"type"=>array(),"size"=>array(),"hide"=>array(),"color"=>array(),"numbers"=>array());
	$eigenval = array("name"=>array(),"x"=>array(),"y"=>array(),"z"=>array());
	$mean = array("name"=>array(),"x"=>array(),"y"=>array(),"z"=>array());
	$eigenval = array("name"=>array(),"x"=>array("x"=>array(),"y"=>array(),"z"=>array()),"y"=>array("x"=>array(),"y"=>array(),"z"=>array()),"z"=>array("x"=>array(),"y"=>array(),"z"=>array()));
	while(!feof($file)){		
		$line=fgets($file);
		
		//check if the line begins with ">" -> name
		//check if the line is in the <pos> tag
		if(strcmp(trim($line),"</seq>")==0){
			$startseq=0;
		}
		if($startseq==1){
			$checkline=str_split($line);
			if(strcmp($checkline[0],">")==0){
				$seqname[$i9]=$line;
			}
			else{$seq[$i9]=$line; $i9++;}
			
		}
		if(strcmp(trim($line),"<seq>")==0){
			$startseq=1;
		}
		//check if the line is in the </seqgroups> tag
		if(strcmp(trim($line),"</seqgroups>")==0){
			$startgrp=0;
		}	
		
		//add the lines to a multidimensional array
		if($startgrp==1){
			$checkline=explode("=", $line);			
			if(strcmp(trim($checkline[0]),"name")==0){
				$seqgrp["name"][$i3] = $checkline[1];
			}
			elseif(strcmp(trim($checkline[0]),"type")==0){
				$seqgrp["type"][$i3] = $checkline[1];
			}
			elseif(strcmp(trim($checkline[0]),"size")==0){
				$seqgrp["size"][$i3] = $checkline[1];
			}
			elseif(strcmp(trim($checkline[0]),"hide")==0){
				$seqgrp["hide"][$i3] = $checkline[1];
			}
			elseif(strcmp(trim($checkline[0]),"color")==0){
				$seqgrp["color"][$i3] = $checkline[1];
			}
			elseif(strcmp(trim($checkline[0]),"numbers")==0){
				$seqgrp["numbers"][$i3] = $checkline[1];
			$i3++;
			}
			
			
		}
		if(strcmp(trim($line),"<seqgroups>")==0){
			$startgrp=1;
		}
		
		//check if the line is in the <pos> tag
		if(strcmp(trim($line),"</pos>")==0){
			$startpos=0;
			//calculate the center of all points
			$addx=0;
			$addy=0;
			$addz=0;
			for($pi=0;$pi<count($seqpos);$pi++){
				$splitpoint = explode(" ",$seqpos[$pi]);
				$addx = $addx+$splitpoint[1];
				$addy = $addy+$splitpoint[2];
				$addz = $addz+$splitpoint[3];
			}
			$addx=$addx/count($seqpos);
			$addy=$addy/count($seqpos);
			$addz=$addz/count($seqpos);
			$centerpoint[0] = $addx;
			$centerpoint[1] = $addy;
			$centerpoint[2] = $addz;
		}	
		if($startpos==1){
			//add every line while in the <pos> tag to the $seqpos array
			$seqpos[$i2]=$line;
			//check every position if in a group or not
			$checkpos=explode(" ",$seqpos[$i2]);
			if(in_array_r($checkpos[0], $numbers, true)==false){
				$seqwogrp[$i8] = $line;
				$i8++;
			}
			$i2++;
		}
		if(strcmp(trim($line),"<pos>")==0){
			$startpos=1;
			//explode numbers and add every one to an array for checking
			for($i7=0;$i7<=count($seqgrp["numbers"])-1;$i7++){
				$newline=explode(";",$seqgrp["numbers"][$i7]);
				$numbers[$i7]=$newline;
			}
		}
		if(strcmp(trim($line),"<seqgroups>")==0){
			$startgrp=1;
		}
		
		//check if the line is in the <eval> tag
		if(strcmp(trim($line),"</eval>")==0){
			$starteval=0;
		}	
		if($starteval==1){
			//add every line while in the <eval> tag to the $eigenval array
			$checkline=explode("=", $line);
			if(strcmp(trim($checkline[0]),"name")==0){
				$eigenval["name"][$i4] = $checkline[1];
			}
			elseif(strcmp(trim($line),"onepoint")==0){
				$i4++;
			}
			elseif(strcmp(trim($checkline[0]),"x")==0){
				$eigenval["x"][$i4] = $checkline[1];
			}
			elseif(strcmp(trim($checkline[0]),"y")==0){
				$eigenval["y"][$i4] = $checkline[1];
			}
			elseif(strcmp(trim($checkline[0]),"z")==0){
				$eigenval["z"][$i4] = $checkline[1];
			$i4++;
			}
		}
		if(strcmp(trim($line),"<eval>")==0){
			$starteval=1;
		}		
		//check if the line is in the <evec> tag
		if(strcmp(trim($line),"</evec>")==0){
			$startevec=0;
		}	
		if($startevec==1){
			//add every line while in the <evec> tag to the $eigenvec array
			$checkline=explode("=", $line);
			if(strcmp(trim($checkline[0]),"name")==0){
				$eigenvec["name"][$i5] = $checkline[1];
			}
			elseif(strcmp(trim($line),"onepoint")==0){
				$i5++;
			}
			elseif(strcmp(trim($checkline[0]),"x")==0){
				$xvec=explode(",", $checkline[1]);
				$eigenvec["x"]["x"][$i5] = $xvec[0];
				$eigenvec["x"]["y"][$i5] = $xvec[1];
				$eigenvec["x"]["z"][$i5] = $xvec[2];
			}
			elseif(strcmp(trim($checkline[0]),"y")==0){
				$yvec=explode(",", $checkline[1]);
				$eigenvec["y"]["x"][$i5] = $yvec[0];
				$eigenvec["y"]["y"][$i5] = $yvec[1];
				$eigenvec["y"]["z"][$i5] = $yvec[2];
			}
			elseif(strcmp(trim($checkline[0]),"z")==0){
				$zvec=explode(",", $checkline[1]);
				$eigenvec["z"]["x"][$i5] = $zvec[0];
				$eigenvec["z"]["y"][$i5] = $zvec[1];
				$eigenvec["z"]["z"][$i5] = $zvec[2];
			$i5++;
			}
		}
		if(strcmp(trim($line),"<evec>")==0){
			$startevec=1;
		}	
		
		if(strcmp(trim($line),"</mean>")==0){
			$startmean=0;
		}	
		if($startmean==1){
			$checkline=explode("=", $line);
			if(strcmp(trim($checkline[0]),"name")==0){
				$mean["name"][$i6] = $checkline[1];
			}
			elseif(strcmp(trim($line),"onepoint")==0){
				$i6++;
			}
			elseif(strcmp(trim($checkline[0]),"x")==0){
				$mean["x"][$i6] = $checkline[1];
			}
			elseif(strcmp(trim($checkline[0]),"y")==0){
				$mean["y"][$i6] = $checkline[1];
			}
			elseif(strcmp(trim($checkline[0]),"z")==0){
				$mean["z"][$i6] = $checkline[1];
			$i6++;
			}
		}
		if(strcmp(trim($line),"<mean>")==0){
			$startmean=1;
		}
	}
}
fclose($file);
?>