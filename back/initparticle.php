<script>
// initparticle.js
// 
// CLANS Web Viewer, an web application for proteinclassification.
// Copyright (C) 2012 Jens Rauch
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program. If not, see http://www.gnu.org/licenses.

/**
 * @author Jens Rauch
 */

if(!Detector.webgl){
      Detector.addGetWebGLMessage();
    } else {
<?php
$legend=array("name"=>array(),"color"=>array(),"ocolor1"=>array(),"ocolor2"=>array(),"ocolor3"=>array());
//function if the file contains groups
if(count($seqgrp["numbers"])>0)
{ ?>
	function initparticlesys(add){
		group = new THREE.Object3D();
		objects = [];
		<?php 
		$pgrp=1;
		//set size of particles
		$partsize=0.2;
		$li=0;
		//just use the ones >size=0 && !=hide=1
		for( $i=0; $i<=(count($seqgrp["name"])-1); $i++){
			if($seqgrp["size"][$i]>0 && $seqgrp["hide"][$i]!=1){
				//get colors and devide by 255 to make it fit for three.js
				$colors=explode(";",$seqgrp["color"][$i]);
				$colors[0]=$colors[0]/255;
				$colors[1]=$colors[1]/255;
				$colors[2]=$colors[2]/255;
				$colors[3]=$colors[3]/255;
				$rgbhex=str_pad(dechex($colors[0]*255),2,"0").str_pad(dechex($colors[1]*255),2,"0").str_pad(dechex($colors[2]*255),2,"0");
				//write to legend array for later reuse in the menu
				$legend["name"][$li]=$seqgrp["name"][$i];
				$legend["color"][$li]=$rgbhex;
				$legend["ocolor1"][$li]=$colors[0];
				$legend["ocolor2"][$li]=$colors[1];
				$legend["ocolor3"][$li]=$colors[2];
				$li++;
				//get the numbers from the grp array
				$numbers=explode(";",$seqgrp["numbers"][$i]);
				?>
				geometry<?php echo $pgrp;?> = new THREE.Geometry();
				<?php
				//get coordinates for every point
				if(empty($showsize)!=TRUE){ $partsize=0.025*$seqgrp["size"][$i]; }
				for( $i1=0; $i1<=(count($numbers)-2); $i1++){
				$nr=$numbers[$i1];
				$coords=explode(" ",$seqpos[$nr]); ?>
				geometry<?php echo $pgrp;?>.vertices.push( new THREE.Vector3( <?php echo $coords[1];?>, <?php echo $coords[2];?>, <?php echo $coords[3];?> ));<?php } ?>
				material<?php echo $pgrp;?> = new THREE.ParticleBasicMaterial( { size: <?php echo $partsize;?>, color: 0xffffff, opacity: <?php echo $colors[3];?>  } );
				material<?php echo $pgrp;?>.color.setRGB(<?php echo $colors[0];?>, <?php echo $colors[1];?>, <?php echo $colors[2];?>);
				particles<?php echo $pgrp;?> = new THREE.ParticleSystem( geometry<?php echo $pgrp;?>, material<?php echo $pgrp;?> );
				group.add( particles<?php echo $pgrp;?> );
				objects.push( particles<?php echo $pgrp;?> );
				
				<?php if(empty($showgrps)==TRUE){ ?> 
				
				<?php if(empty($eigenvec["x"]["x"][$pgrp-1])==TRUE){ ?>
					radius=<?php echo $partsize; ?>*0.5;
					sphere<?php echo $pgrp;?> = new THREE.Mesh( new THREE.SphereGeometry(radius,10,10), new THREE.MeshBasicMaterial({ color: <?php echo "0x".$rgbhex;?>, opacity: 0.3, transparent: true}) );
					group.add( sphere<?php echo $pgrp;?> );
					sphere<?php echo $pgrp;?>.position.x=<?php echo $coords[1]; ?>;
					sphere<?php echo $pgrp;?>.position.y=<?php echo $coords[2]; ?>;
					sphere<?php echo $pgrp;?>.position.z=<?php echo $coords[3]; ?>;
					sphere<?php echo $pgrp;?>.material.depthWrite = false;
					sphere<?php echo $pgrp;?>.material.depthTest = false;
				<?php }
				else{ ?>
				var xfactor=3,
				yfactor=xfactor,
				zfactor=xfactor,
				radius=1,
				min=0.02;
				sphere<?php echo $pgrp;?> = new THREE.Mesh( new THREE.SphereGeometry(radius,20,20), new THREE.MeshBasicMaterial({ color: <?php echo "0x".$rgbhex;?>, opacity: 0.3, transparent: true}) );
				group.add( sphere<?php echo $pgrp;?> );
				// objects.push( sphere<?php echo $pgrp;?> );
				<?php
				//calculation of scalingfactors based on how many points there are in one group and based on the eigenvector size
				$maximal = max(abs($eigenval["x"][$pgrp-1]),abs($eigenval["y"][$pgrp-1]),abs($eigenval["z"][$pgrp-1]));
				if(abs($eigenval["x"][$pgrp-1]==$maximal)){
					if($eigenval["x"][$pgrp-1]>=3 && count($numbers)-2<100){
						?> xfactor=1; yfactor = zfactor = xfactor+4; <?php
					}
					elseif($eigenval["x"][$pgrp-1]>=1 && $eigenval["x"][$pgrp-1]<=4 && count($numbers)-2<100){
						?> xfactor=2; zfactor = yfactor = xfactor+3; <?php
					}
					elseif($eigenval["x"][$pgrp-1]>=4 && count($numbers)-2<100){
						?> xfactor=1; zfactor = yfactor = xfactor+3; <?php
					}
					else{
						if(count($numbers)-2>=500 && $eigenval["x"][$pgrp-1]>=2){ ?> xfactor=1; zfactor = yfactor = xfactor+2; <?php }
						elseif(count($numbers)-2>=250 && $eigenval["x"][$pgrp-1]>=2 && $eigenval["x"][$pgrp-1]<=10){ ?> xfactor=2; zfactor = yfactor = xfactor+2; <?php }
						elseif(count($numbers)-2>=100 && $eigenval["x"][$pgrp-1]>=2 && $eigenval["x"][$pgrp-1]<=10){ ?> xfactor=3; zfactor = yfactor = xfactor+4; <?php }
						elseif(count($numbers)-2>=100 && $eigenval["x"][$pgrp-1]>=10){ ?> xfactor=1; zfactor = yfactor = xfactor+1; <?php }
						else{ ?> xfactor=5; zfactor = yfactor = xfactor+4; <?php }
					}
				}
				elseif(abs($eigenval["y"][$pgrp-1]==$maximal)){
					if($eigenval["y"][$pgrp-1]>=3 && count($numbers)-2<10){
						?> yfactor=1; xfactor = zfactor = yfactor+4; <?php
					}
					elseif($eigenval["y"][$pgrp-1]>=1 && $eigenval["y"][$pgrp-1]<=4 && count($numbers)-2<100){
						?> yfactor=2; xfactor = zfactor = yfactor+3; <?php
					}
					elseif($eigenval["y"][$pgrp-1]>=4 && count($numbers)-2<100){
						?> yfactor=1; xfactor = zfactor = yfactor+3; <?php
					}
					else{
						if(count($numbers)-2>=500 && $eigenval["y"][$pgrp-1]>=2){ ?> yfactor=1; xfactor = zfactor = yfactor+2; <?php }
						elseif(count($numbers)-2>=250 && $eigenval["y"][$pgrp-1]>=2 && $eigenval["y"][$pgrp-1]<=10){ ?> yfactor=2; xfactor = zfactor = yfactor+2; <?php }
						elseif(count($numbers)-2>=100 && $eigenval["y"][$pgrp-1]>=2 && $eigenval["y"][$pgrp-1]<=10){ ?> yfactor=3; xfactor = zfactor = yfactor+4; <?php }
						elseif(count($numbers)-2>=100 && $eigenval["y"][$pgrp-1]>=10){ ?> yfactor=1; xfactor = zfactor = yfactor+1; <?php }
						else{ ?> yfactor=5; xfactor = zfactor = yfactor+4; <?php }
					}
				}
				elseif(abs($eigenval["z"][$pgrp-1]==$maximal)){
					if($eigenval["z"][$pgrp-1]>=3 && count($numbers)-2<10){
						?> zfactor=1; xfactor = yfactor = zfactor+4; <?php
					}
					elseif($eigenval["z"][$pgrp-1]>=1 && $eigenval["z"][$pgrp-1]<=4 && count($numbers)-2<100){
						?> zfactor=2; xfactor = yfactor = zfactor+3; <?php
					}
					elseif($eigenval["z"][$pgrp-1]>=4 && count($numbers)-2<100){
						?> zfactor=1; xfactor = yfactor = zfactor+3; <?php
					}
					else{
						if(count($numbers)-2>=500 && $eigenval["z"][$pgrp-1]>=2){ ?> zfactor=1; xfactor = yfactor = zfactor+2; <?php }
						elseif(count($numbers)-2>=250 && $eigenval["z"][$pgrp-1]>=2 && $eigenval["z"][$pgrp-1]<=10){ ?> zfactor=2; xfactor = yfactor = zfactor+2; <?php }
						elseif(count($numbers)-2>=100 && $eigenval["z"][$pgrp-1]>=2 && $eigenval["z"][$pgrp-1]<=10){ ?> zfactor=3; xfactor = yfactor = zfactor+4; <?php }
						elseif(count($numbers)-2>=100 && $eigenval["z"][$pgrp-1]>=10){ ?> zfactor=1; xfactor = yfactor = zfactor+1; <?php }
						else{ ?> zfactor=5; xfactor = yfactor = zfactor+4; <?php }
					}
				}
				//if eigenval is greater than 10 reduce the factor by a certain value
				if($eigenval["x"][$pgrp-1]>10){ ?> xfactor=xfactor*0.4; <?php }
				if($eigenval["y"][$pgrp-1]>10){ ?> yfactor=yfactor*0.4; <?php }
				if($eigenval["z"][$pgrp-1]>10){ ?> zfactor=zfactor*0.4; <?php }
				if($eigenval["x"][$pgrp-1]<0.1){ ?> xfactor=xfactor*2; <?php }
				if($eigenval["y"][$pgrp-1]<0.1){ ?> yfactor=yfactor*2; <?php }
				if($eigenval["z"][$pgrp-1]<0.1){ ?> zfactor=zfactor*2; <?php }
				
				//if eigenvalue is too small set it to minimal size
				if($eigenval["x"][$pgrp-1]<=0.02){ ?> sphere<?php echo $pgrp;?>.scale.x = min*xfactor; <?php }
				else { ?>  sphere<?php echo $pgrp;?>.scale.x = Math.abs(<?php echo $eigenval["x"][$pgrp-1]; ?>)*xfactor; <?php }
				if($eigenval["y"][$pgrp-1]<=0.02){ ?> sphere<?php echo $pgrp;?>.scale.y = min*yfactor; <?php }
				else { ?>  sphere<?php echo $pgrp;?>.scale.y = Math.abs(<?php echo $eigenval["y"][$pgrp-1]; ?>)*yfactor; <?php }
				if($eigenval["z"][$pgrp-1]<=0.02){ ?> sphere<?php echo $pgrp;?>.scale.z = min*zfactor; <?php }
				else { ?>  sphere<?php echo $pgrp;?>.scale.z = Math.abs(<?php echo $eigenval["z"][$pgrp-1]; ?>)*zfactor; <?php }
				// WARNING: Modyfied three.js for use with second vector for parameter up (line 152)
				// you have to use the supplied three.js version
				// use: lookAt(lookatvector, upvector);
				// code: lookAt:function(){var a=new THREE.Matrix4;return function(b,c){if(typeof(c)=="undefined"){c=this.up};a.lookAt(b,this.position,c);this.quaternion.setFromRotationMatrix(a)}}(), ?>
				sphere<?php echo $pgrp;?>.lookAt(new THREE.Vector3(<?php echo $eigenvec["z"]["x"][$pgrp-1]; ?>,<?php echo $eigenvec["z"]["y"][$pgrp-1]; ?>,<?php echo $eigenvec["z"]["z"][$pgrp-1]; ?>),new THREE.Vector3(<?php echo $eigenvec["y"]["x"][$pgrp-1]; ?>,<?php echo $eigenvec["y"]["y"][$pgrp-1]; ?>,<?php echo $eigenvec["y"]["z"][$pgrp-1]; ?>));
				group.add( sphere<?php echo $pgrp;?> );
				sphere<?php echo $pgrp;?>.position.x=<?php echo $mean["x"][$pgrp-1]; ?>;
				sphere<?php echo $pgrp;?>.position.y=<?php echo $mean["y"][$pgrp-1]; ?>;
				sphere<?php echo $pgrp;?>.position.z=<?php echo $mean["z"][$pgrp-1]; ?>;
				sphere<?php echo $pgrp;?>.material.depthWrite = false;
				sphere<?php echo $pgrp;?>.material.depthTest = false;
				<?php }
				?> wsphere<?php echo $pgrp;?> = new THREE.Mesh( new THREE.SphereGeometry(radius,20,20), new THREE.MeshBasicMaterial({ color: 0x000000, wireframe: true}) ); 
				wsphere<?php echo $pgrp;?>.scale=sphere<?php echo $pgrp;?>.scale.clone();
				<?php if(empty($eigenvec["x"]["x"][$pgrp-1])==FALSE){ ?>
					wsphere<?php echo $pgrp;?>.lookAt(new THREE.Vector3(<?php echo $eigenvec["z"]["x"][$pgrp-1]; ?>,<?php echo $eigenvec["z"]["y"][$pgrp-1]; ?>,<?php echo $eigenvec["z"]["z"][$pgrp-1]; ?>),new THREE.Vector3(<?php echo $eigenvec["y"]["x"][$pgrp-1]; ?>,<?php echo $eigenvec["y"]["y"][$pgrp-1]; ?>,<?php echo $eigenvec["y"]["z"][$pgrp-1]; ?>));
				<?php } ?>
				wsphere<?php echo $pgrp;?>.position=sphere<?php echo $pgrp;?>.position.clone();
				<?php
				}
				$pgrp++;
			}
		}
		?> geometryblack = new THREE.Geometry(); <?php
		for( $i=0; $i<=(count($seqwogrp)-1); $i++){ $coords=explode(" ",$seqwogrp[$i]); ?> geometryblack.vertices.push( new THREE.Vector3( <?php echo $coords[1];?>, <?php echo $coords[2];?>, <?php echo $coords[3];?> 
			)); <?php } ?>
		materialblack = new THREE.ParticleBasicMaterial( { size: 0.25*<?php echo $partsize;?>, color: 0x000000 } );
		particlesblack = new THREE.ParticleSystem( geometryblack, materialblack );
		group.add( particlesblack );
		if(add==true){scene.add( group );}
	}
<?php }
//function if the file does not contain groups
else{ ?>
	function initparticlesys(){
 
		geometry = new THREE.Geometry();
		//get coordinates for every point
		<?php for ( $i=0; $i<=($seqcount[1]-1); $i++ ) {
			$coords=explode(" ",$seqpos[$i]); ?>
			geometry.vertices.push( new THREE.Vector3( <?php echo $coords[1];?>, <?php echo $coords[2];?>, <?php echo $coords[3];?> ));<?php } ?>
			material = new THREE.ParticleBasicMaterial( { size: 1, color: 0x0033CC } );
			particles = new THREE.ParticleSystem( geometry, material );
			particles.sortParticles = true;
			if(add==true){scene.add( particles );}
	}
<?php }
?>
}
</script>
