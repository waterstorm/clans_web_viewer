<?php
// hmmer.php
// 
// CLANS Web Viewer, an web application for proteinclassification.
// Copyright (C) 2012 Jens Rauch
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program. If not, see http://www.gnu.org/licenses.
include("../config.php");
$ufile = substr($_POST["a"],0,-4);
$check = $_POST["b"];
$pid = $_POST["c"];
$thisdir = $_POST["d"];
$ft = $_POST["e"];
function run_in_background($command){
	$thisdir = $_POST["d"];
	$tempfile = $thisdir.'/hmmer.temp';
	$PID = system( 'nice '.$command.' >'.$tempfile.' 2>&1 & echo $!' );
	return($PID);
}
function is_process_running($PID){
	exec("ps $PID", $ProcessState);
	echo count($ProcessState) >= 2;
}
function kill_process($PID){
	exec("kill $PID");
}
//check 0: execute hmmer
if($check==0){
	$tmp=explode("/",$ufile);
	$exists=array_pop($tmp);
	$copypath="";
	array_pop($tmp);
	array_pop($tmp);
	foreach($tmp as $value){
		$copypath=$copypath."/".$value;
	}	
	if(file_exists($copypath."/ftpuploads/".$exists.".hmm")){
		$PID = system( 'mv '.$copypath.'/ftpuploads/'.$exists.'.hmm '.$ufile.'.hmm 2>&1 & echo $!' );
		echo("<p>This HMM has been provided. Skipping...</p>");
		return($PID);
	}
	else{
		if($cpucores=="default"){ $command = "/usr/bin/hmmbuild --fragthres ".$ft." --informat afa ".$ufile.".hmm ".$ufile.".aln"; }
		else{ $command = "/usr/bin/hmmbuild --fragthres ".$ft." --cpu ".$cpucores." --informat afa ".$ufile.".hmm ".$ufile.".aln"; }
		$PID = run_in_background($command);
	}
	unset($tmp);
	unset($exists);
}
//check 1: stop muscle if script was stoped
elseif($check==1){
	kill_process($pid);
}
elseif($check==2){ chmod($ufile.".hmm", 0777); }

elseif($check==3){
	//combine all hmm files to one hmm database
	shell_exec("find $thisdir/ -not -name 'background*' -name '*.hmm' -exec cat {} + > $thisdir/hmmdb");
	chmod($thisdir."/hmmdb", 0777);
	shell_exec("/usr/bin/hmmpress $thisdir/hmmdb");
	shell_exec("/usr/bin/hmmpress $thisdir/background.hmm");
	
}
elseif($check==4){
	is_process_running($pid);
}
?>