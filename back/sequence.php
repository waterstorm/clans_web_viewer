<?php
// sequence.php
// 
// CLANS Web Viewer, an web application for proteinclassification.
// Copyright (C) 2012 Jens Rauch
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program. If not, see http://www.gnu.org/licenses.
$seq = $_POST["a"];
$dir = $_POST["b"];
$wfile = $_POST["c"];
$wfilename = $_POST["d"];
$case = $_POST["e"];
if($case=="seqadd"){
	if($wfile=="false"){
		$number=0;
		$tempf = $dir."/newseq".$number.".txt";
		$tempo = $dir."/output".$number.".txt";
		//create new file for the input to use hmmerscan and check for duplicates
		while(file_exists($tempf)==TRUE){$number++; $tempf = $dir."/newseq".$number.".txt"; $tempo = $dir."/output".$number.".txt";}
		if (!$handle = fopen($tempf, "x")){
			echo "cannot open file ".$tempf."<br>";
			exit;
		}
		if(!fwrite($handle, $seq)){
			echo "cannot write to file<br>";
			exit;
		}
		else{chmod($tempf, 0777); fclose($handle);}
		//execute hmmerscan to analyse the input
		exec("nice /usr/bin/hmmscan ".$dir."/hmmdb ".$tempf ,$log,$log2);
		if($log2!=0){echo "!!Error!! An Error occured! Please check your input for FASTA format!<br>"; exit; }
		else if($log2==0){
			while(file_exists($tempo)==TRUE){$number++; $tempo = $dir."/output".$number.".txt";}
			if (!$handle = fopen($tempo, "x")){
				echo "cannot open file ".$tempo."<br>";
				exit;
			}
		}
		//write array $log (with hmmer output) to file
		$out = "";
		$Lines = count($log);
		for ($i=0; $i<$Lines; $i++){
		$out .= "$log[$i]\n" ;
		}
		if(!fwrite($handle, $out)){
			echo "cannot write to file<br>";
		exit;
		}
		else{chmod($tempo, 0777); fclose($handle);}
	}
	else{
		$tempo=$dir."/".$wfilename;
		$file=fopen($tempo, "r") or exit("Unable to open file!");
		$i=0;
		while(!feof($file)){
			$line=fgets($file);
			$log[$i] = $line;
			$i++;
		}
		fclose($file);
	}
	//analyse the output of hmmerscan
	$preoutput="";
	for($i=0;$i<=count($log)-1;$i++){
		$exp = explode(" ",$log[$i]);
		if($exp[0]=="Query:"){
			$output="$tempo\n";
			$queryl = explode(" ", $log[$i]);
			//remove the length indicator from hmmer
			$query="";
			for($i1=0;$i1<=count($queryl)-1;$i1++){
				$pos = strpos($queryl[$i1], "[L=");
				if($pos === false){$query.=$queryl[$i1]." ";}
			}
			//remove the "Query:" and the "Description"-tag if available
			$query = preg_replace ('#\s+#' , ' ' , $query);
			$query = explode(":", $query);
			for($qi=1;$qi<count($query);$qi++){
				$output .= $query[$qi];
			}
			//check for description tag 
			$query = preg_replace ('#\s+#' , ' ' , $log[$i+1]);
			$query = explode(":", $query);
			$descavail = false;
			if($query[0]=="Description"){
				for($qi=1;$qi<count($query);$qi++){
					$output .= $query[$qi];
				}
				$descavail = true;
			}
			$output .= "\n";
			//check for hits, if no hits print it out
			//[No hits detected that satisfy reporting thresholds]
			$nohits = "[No hits detected that satisfy reporting thresholds]";
			($descavail) ? $hit = strpos($log[$i+7], $nohits) : $hit = strpos($log[$i+6], $nohits);
			if($hit!==false){
				$preoutput .= "1"."\n".$output;
				$preoutput .= "Warning:\nNo hits detected that satisfy reporting thresholds\n";
			}
			else{
			//get E-Vals and do neg dec log
			($descavail) ? $preeval = preg_replace ('#\s+#' , ' ' , $log[$i+6]) : $preeval = preg_replace ('#\s+#' , ' ' , $log[$i+5]);
			$eval = explode(" ",$preeval);
			$ix=0;
			while((count($eval)>2)==TRUE){
				if($eval[1]!=0){ $output .= -log($eval[1],10)."\n"; }
				else{ $output .= -log(5e-310,10)."\n"; }
				$output .= $eval[9]."\n";
				$ix++;
				($descavail) ? $preeval = preg_replace ('#\s+#' , ' ' , $log[$i+6+$ix]) : $preeval = preg_replace ('#\s+#' , ' ' , $log[$i+5+$ix]);
				$eval = explode(" ",$preeval);
			}
			$preoutput .= $ix."\n".$output;
			}
		}
	}
	echo $preoutput;
}
elseif($case=="addtofile"){
	$file = $dir."/".$wfilename; 
	$handle=fopen($file, "a+") or exit("Unable to open file!");
	$file2 = $dir."/".$wfile;
	$handle2 = file_get_contents($file2);
	if(!fwrite($handle, $handle2)){
		echo "cannot write to file<br>";
		exit;
	}
	else{
		fclose($handle);
		unlink($file2);
	}
}
?>