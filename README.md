CLANS Web Viewer
================

#### Running example ####

A running example of the current git code (or possible future versions) can be viewed [here](http://134.34.129.14/).
Please do not upload files on this test page. If you want to do this, you should install the program on your server/computer.
The version linked above is running on a local computer not on a server! Therefore if it needs to reboot or if it crashes the website may not be available for some time.

#### Installation ####

A manual how to install and use this application can also be found in this [git repository](https://bitbucket.org/waterstorm/clans_web_viewer/src/master/clans_web_viewer-help.pdf).
