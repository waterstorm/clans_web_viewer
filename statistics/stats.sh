#!/bin/bash

if [ -z "$1" ]; then
  echo 'stats.sh needs at least one argument!'  
  echo 'stats.sh filename iterations (optional,default = 100) startingpercentage (optional,default = 10)'
  exit 0
fi

if [ -z "$2" ]; then
  $2 = 100
fi

if [ -z "$3" ]; then
  $3 = 10
fi

for (( i=$3; i<100; i=$((i+10)) ))
do
  for (( j=1; j<=$2; j++ ))
  do
	  php hox_cli.php $1 ${i} ${j}
  done
done

cat ./statistics/*.txt > ./statistics/all.csv