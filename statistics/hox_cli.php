<?php
// hox_cli.php
// 
// CLANS Web Viewer, an web application for proteinclassification.
// Copyright (C) 2012 Jens Rauch
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program. If not, see http://www.gnu.org/licenses.

if(isset($argv[1])!=true){
	echo "you have not specified which file to use. Please use \"php hox_cli.php filename\"";
}
$filename = $argv[1];
$ddir = "./temp/";
$fpath = $ddir.$filename;
$file=fopen($filename,"r") or exit("Unable to open file!");
if($file){
	//remove <hsp> part of the file (not needed and VERY large) and save it as a temp file, remove original
	if(file_exists($fpath)==FALSE){
	    $buffer="";
		$i=0;
	    while(!feof($file)) {
	    	$line=fgets($file);	
			$buffer.= $line;
	    	if(strcmp(trim($line),"</pos>")!=0){
	    		if($i==0){
	    			echo "Processing file...\n";
					$i++;
				}
				else {
					$i++;
				}				
			}
			else{
				if (!$handle = fopen($fpath, "w"))
				{
					echo "cannot open file\n";
					exit;
				}
				if(!fwrite($handle, $buffer))
				{
					echo "cannot open file\n";
					exit;
				}
				fclose($handle);
				echo "Done!\n";
				break;
			}
		}
	}
}
fclose($file);
echo "calculating a PCA please wait...\n";
include("./back/pca.php");
//get the sequence positions
$file=fopen($fpath, "r") or exit("Unable to open file!");
if($file){
	//get first line (Sequences)
	$line=fgets($file);
	$seqcount=explode("=",$line,2);
	$i=0;
	$i2=0;
	$i3=0;
	$i9=0;
	$startpos=0;
	$startseq=0;
	$startgrp=0;
	$seqgrp = array("name"=>array(),"type"=>array(),"size"=>array(),"hide"=>array(),"color"=>array(),"numbers"=>array());
	while(!feof($file)){		
		$line=fgets($file);
		
		//check if the line begins with ">" -> name
		//check if the line is in the <pos> tag
		if(strcmp(trim($line),"</seq>")==0){
			$startseq=0;
		}
		if($startseq==1){
			$checkline=str_split($line);
			if(strcmp($checkline[0],">")==0){
				$seqname[$i9]=$line;
			}
			else{
				//check if line conains more than just the sequence
				$secu = explode(" ", $line);
				if(count($secu)>2){echo "Error: Your sequence line contains more than just the sequence! Please check it.\n"; exit;}
				else{$seq[$i9]=str_replace(" ","",$line); $i9++;} 
			}
			
		}
		if(strcmp(trim($line),"<seq>")==0){
			$startseq=1;
		}
		
		//check if the line is in the </seqgroups> tag
		if(strcmp(trim($line),"</seqgroups>")==0){
			$startgrp=0;
		}	
		
		//add the lines to a multidimensional array
		if($startgrp==1){
			$checkline=explode("=", $line);			
			if(strcmp(trim($checkline[0]),"name")==0){
				$seqgrp["name"][$i3] = $checkline[1];
			}
			elseif(strcmp(trim($checkline[0]),"type")==0){
				$seqgrp["type"][$i3] = $checkline[1];
			}
			elseif(strcmp(trim($checkline[0]),"size")==0){
				$seqgrp["size"][$i3] = $checkline[1];
			}
			elseif(strcmp(trim($checkline[0]),"hide")==0){
				$seqgrp["hide"][$i3] = $checkline[1];
			}
			elseif(strcmp(trim($checkline[0]),"color")==0){
				$seqgrp["color"][$i3] = $checkline[1];
			}
			elseif(strcmp(trim($checkline[0]),"numbers")==0){
				$seqgrp["numbers"][$i3] = $checkline[1];
			$i3++;
			}			
		}
		if(strcmp(trim($line),"<seqgroups>")==0){
			$startgrp=1;
		}
		
		//check if the line is in the <pos> tag
		if(strcmp(trim($line),"</pos>")==0){
			$startpos=0;
		}	
		if($startpos==1){
			//add every line while in the <pos> tag to the $seqpos array
			$seqpos[$i2]=$line;
			$i2++;
		}
		if(strcmp(trim($line),"<pos>")==0){
			$startpos=1;
		}					
	}
}
fclose($file);

if (!$handle = fopen($fpath, "a")){
	echo "cannot open file ".$fpath."\n";
	exit;
}
else{
	//write mean, eigenvalue and eigenvectors in the temp file
	if(count($seqgrp["numbers"])>0)
	{
		$buffermean="<mean>"."\r\n";
		$bufferval="<eval>"."\r\n";
		$buffervec="<evec>"."\r\n";
		//just use the ones >size=0 && !=hide=1
		for( $i=0; $i<=(count($seqgrp["name"])-1); $i++){
			if($seqgrp["size"][$i]>0 && $seqgrp["hide"][$i]!=1){
				//get the numbers from the grp array
				$numbers=explode(";",$seqgrp["numbers"][$i]);
				//get coordinates for every point	
				for( $i1=0; $i1<=(count($numbers)-2); $i1++){
					$nr=$numbers[$i1];
					$coords=explode(" ",$seqpos[$nr]);
					$vec["x"][$i1]=$coords[1];
					$vec["y"][$i1]=$coords[2];
					$vec["z"][$i1]=$coords[3];
				}
				if((count($numbers)-1)>1){
					$meanx=0;
					$meany=0;
					$meanz=0;
					$eigenvecl = array();
					if(empty($eigenvecl)!=TRUE){unset($eigenvecl);}
					pca($vec);
					for($i2=-1;$i2<=count($eigenval)-1;$i2++){
						if($i2==-1){ $bufferval.="name=".$seqgrp["name"][$i]; }
						elseif($i2==0){ $bufferval.="x=".$eigenval[$i2][0]."\r\n"; }
						elseif($i2==1){ $bufferval.="y=".$eigenval[$i2][0]."\r\n"; }
						elseif($i2==2){ $bufferval.="z=".$eigenval[$i2][0]."\r\n"; }
					}
					for($i3=-1;$i3<=2;$i3++){
						if($i3==-1){$buffervec.="name=".$seqgrp["name"][$i];}
						elseif($i3==0){ $buffervec.="x=".$eigenvecl["0"][$i3]["0"].",".$eigenvecl["1"][$i3]["0"].",".$eigenvecl["2"][$i3]["0"]."\r\n"; }
						elseif($i3==1){ $buffervec.="y=".$eigenvecl["0"][$i3]["0"].",".$eigenvecl["1"][$i3]["0"].",".$eigenvecl["2"][$i3]["0"]."\r\n"; }
						elseif($i3==2){ $buffervec.="z=".$eigenvecl["0"][$i3]["0"].",".$eigenvecl["1"][$i3]["0"].",".$eigenvecl["2"][$i3]["0"]."\r\n"; }
					}
					for($i2=-1;$i2<=2;$i2++){
						if($i2==-1){ $buffermean.="name=".$seqgrp["name"][$i]; }
						elseif($i2==0){ $buffermean.="x=".$meanx."\r\n"; }
						elseif($i2==1){ $buffermean.="y=".$meany."\r\n"; }
						elseif($i2==2){ $buffermean.="z=".$meanz."\r\n"; }
					}
					unset($vec);
				}
				else{
					//check if the group contains only one point
					$buffermean.="name=".$seqgrp["name"][$i];
					$buffermean.="onepoint"."\r\n";
					$buffervec.="name=".$seqgrp["name"][$i];
					$buffervec.="onepoint"."\r\n";
					$bufferval.="name=".$seqgrp["name"][$i];
					$bufferval.="onepoint"."\r\n";
					
				}
				
			}
		}
	$buffermean.="</mean>"."\r\n";
	$bufferval.="</eval>"."\r\n";
	$buffervec.="</evec>"."\r\n";
	//write to file
	if(!fwrite($handle, $buffermean)){
					echo "cannot write to file\n";
					exit;
	}
	if(!fwrite($handle, $bufferval)){
					echo "cannot write to file\n";
					exit;
	}
	if(!fwrite($handle, $buffervec)){
					echo "cannot write to file\n";
					exit;
	}
	}
fclose($handle);
}
echo "PCA done and written to temp file.\n";
echo "taking out ".$argv[2]." percent of sequences from each group\n";
echo "calculating...\n";
$numbersog=array(array());
$i3=0;
for($i=0;$i<count($seqgrp["name"])-1;$i++){
	$numbersog[$i] = explode(";",$seqgrp["numbers"][$i]);
	array_pop($numbersog[$i]);
	if(count($numbersog[$i])>=50){
		$numbertotake=floor($argv[2]/100*count($numbersog[$i]));
		$takenout=array();
		for($i2=0;$i2<$numbertotake;$i2++){
			$take = array_rand($numbersog[$i]);
			$takenout[$i2] = $numbersog[$i][$take];
			unset($numbersog[$i][$take]);
		}
		//write takenout numbers to file
		$newfile = $ddir."takenout".$i.".take";
		$grpcount[$i3]=$newfile;
		$i3++;
		$grpcount[$i3]=$i;
		$i3++;
		echo $newfile;
		if (!$handle = fopen($newfile, "a")){
			echo "cannot open file ".$newfile."\n";
			exit;
		} else{
			$output = "";
			foreach($takenout as $value){
				$output .= $seqname[$value];
				$output .= $seq[$value];
			}
			if(!fwrite($handle, $output)){
				echo "cannot write to file\n";
				exit;
			} else { echo "written taken out numbers to file\n"; }
		}
		fclose($handle);
	}
}
echo "reading clans temp file again\n";
if (!$handle = fopen($fpath, "r")){
	echo "cannot open file ".$fpath."\n";
	exit;
} else{
	if($handle){			
		$buffer="";
	    while(!feof($handle)) {
	    	$line=fgets($handle);	
			$buffer.= $line;
		}
	}
}
fclose($handle);
for($i=0;$i<count($seqgrp["name"])-1;$i++){
	if(count($numbersog[$i])>=50){
		$newline="numbers=";
		foreach($numbersog[$i] as $value){
			$newline .= $value.";";
		}
		$newline .= "\n";
		$oldline = "numbers=".$seqgrp["numbers"][$i];
		$buffer = str_replace($oldline, $newline, $buffer);
	}
}
echo "changing values and saving for HMM creation\n";
if (!$handle = fopen($fpath, "w")){
	echo "cannot open file ".$fpath."\n";
	exit;
} else{
	if(!fwrite($handle, $buffer)){
		echo "cannot write to file\n";
		exit;
	} else { echo "written new clans temp file\n"; }
}
fclose($handle);
echo "Creating group files for HMM\n";
//get the directory and create a new folder
$dirname = substr($filename,0,-4);
$thisdir = "./temp/".$dirname;
$hm=0;
for($i=0;$i<=(count($seqgrp["name"])-1);$i++){
	if($seqgrp["size"][$i]>0 && $seqgrp["hide"][$i]!=1){
		$hm++;
	}
}
if(mkdir($thisdir , 0777)){ chmod($thisdir, 0777); echo "Directory has been created successfully\n"; }
else{ echo "Failed to create directory\n";}
$i3=0;
for($i=0;$i<=(count($seqgrp["name"])-1);$i++){
	if($seqgrp["size"][$i]>0 && $seqgrp["hide"][$i]!=1){
		$chars = array(";", "/");
		$fixedgrp = str_replace($chars, "", $seqgrp["name"][$i]);
		$ufile = $thisdir."/".trim($fixedgrp).".txt";
		if($i3<=$hm){ $usedgrps[$i3]=$fixedgrp; }
		$i3++;
		if (!$handle = fopen($ufile, "w")){
			echo"cannot open file ".$ufile."\n";
			exit;
		}
		else{
			chmod($ufile, 0777);
			//get the numbers from the grp array
			$numbers=explode(";",$seqgrp["numbers"][$i]);
			//get names and seqence for every point
			$buffer = "";
			for( $i2=0; $i2<=(count($numbers)-2); $i2++){
			$nr=$numbers[$i2];
			$buffer.=">".$nr."\n";
			$buffer.=$seq[$nr];;
			}
		}
		if(!fwrite($handle, $buffer)){
		echo "cannot write to file\n";
		exit;
		}
		if (!$handle2 = fopen($ufile, "r")){
			echo "cannot open file ".$ufile."\n";
			exit;
		}
		else{
			$donotwrite=0;
			while(!feof($handle2)){
				$line=fgets($handle2);
				$checkline=str_split($line);
				if(strcmp($checkline[0],">")==0){
					$donotwrite++;
				}
			}
		fclose($handle2);
		$hmm[$i3]=$donotwrite;
		}
	}
	if($i==(count($seqgrp["name"])-1)){
		shell_exec("cat $thisdir/*.txt > $thisdir/background.txt");
	}
}
//create alignments
for($i=0;$i<count($seqgrp["name"])-1;$i++){
	$fixedgrp = str_replace($chars, "", $seqgrp["name"][$i]);
	$ufile = $thisdir."/".trim($fixedgrp);
	echo "\n\nworking on file".$ufile."\n";
	system( 'nice /usr/bin/muscle -in '.$ufile.'.txt -out '.$ufile.'.aln' );
}
$ufile = $thisdir."/background";
echo "\n\nworking on file".$ufile."\n";
// system( 'nice /usr/bin/muscle -in '.$ufile.'.txt -out '.$ufile.'.aln' );
echo "\n\nfinished creating alignments\nstart building HMMs\n";
for($i=0;$i<count($seqgrp["name"])-1;$i++){
	$fixedgrp = str_replace($chars, "", $seqgrp["name"][$i]);
	$ufile = $thisdir."/".trim($fixedgrp);
	echo "\n\nworking on file".$ufile."\n";
	system( 'nice /usr/bin/hmmbuild --fragthresh 0 --informat afa '.$ufile.'.hmm '.$ufile.'.aln' );
}
$ufile = $thisdir."/background";
echo "\n\nworking on file".$ufile."\n";
// system( 'nice /usr/bin/hmmbuild --informat afa '.$ufile.'.hmm '.$ufile.'.aln' );
shell_exec("find $thisdir/ -not -name 'background*' -name '*.hmm' -exec cat {} + > $thisdir/hmmdb");
chmod($thisdir."/hmmdb", 0777);
shell_exec("/usr/bin/hmmpress $thisdir/hmmdb");
// shell_exec("/usr/bin/hmmpress $thisdir/background.hmm");
echo "\n\nfinished creating HMMs proceeding with tests\n";
echo "executing hmmscan\n";
$puffer="";
$ocorrect=0;
$oincorrect=0;
for($ia=0;$ia<(count($grpcount))/2;$ia++){
	unset($log);
	unset($log2);
	$tempo = $thisdir."/output".$ia.".txt";
	exec("nice /usr/bin/hmmscan ".$thisdir."/hmmdb ".$grpcount[$ia*2] ,$log,$log2);
	if($log2!=0){echo "!!Error!! An Error occured! Please check your input for FASTA format!\n"; exit; }
	else if($log2==0){
		$tempo = $ddir."output".$ia.".txt";
		if (!$handle = fopen($tempo, "x")){
			echo "cannot open file ".$tempo."\n";
			exit;
		}
	}
	echo "write hmmer output to file\n";
	$out = $grpcount[(($ia+1)*2)-1]."\n";
	$Lines = count($log);
	for ($i=0; $i<$Lines; $i++){
	$out .= "$log[$i]\n" ;
	}
	if(!fwrite($handle, $out)){
		echo "cannot write to file<br>";
	exit;
	}
	else{chmod($tempo, 0777); fclose($handle);}
	echo "processsing hmmer output\n";
	//analyse the output of hmmerscan
	$output="";
	$hmmi=0;
	$hmmname=array();
	$hmmeval=array();
	$hmmgroup=array();
	for($i=0;$i<=count($log)-1;$i++){
		$exp = explode(" ",$log[$i]);
		if($exp[0]=="Query:"){
			$queryl = explode(" ", $log[$i]);
			//remove the length indicator from hmmer
			$query="";
			for($i1=0;$i1<=count($queryl)-1;$i1++){
				$pos = strpos($queryl[$i1], "[L=");
				if($pos === false){$query.=$queryl[$i1]." ";}
			}
			//remove the "Query:" and the "Description"-tag
			$query = preg_replace ('#\s+#' , ' ' , $query);
			$query = explode(":", $query);
			for($qi=1;$qi<count($query);$qi++){
				$output .= ">".trim($query[$qi]);
			}
			$query = preg_replace ('#\s+#' , ' ' , $log[$i+1]);
			$query = explode(":", $query);
			for($qi=1;$qi<count($query);$qi++){
				$output .= $query[$qi];
			}
			$hmmname[$hmmi]=$output."\n";
			//check for hits, if no hits print it out
			//[No hits detected that satisfy reporting thresholds]
			$nohits = "[No hits detected that satisfy reporting thresholds]";
			$hit = strpos($log[$i+6], $nohits);
			if($hit!==false){
				$hmmeval[$hmmi] = "Warning:\nNo hits detected that satisfy reporting thresholds\n";
			}
			else{
				$preeval = preg_replace ('#\s+#' , ' ' , $log[$i+6]);
				$eval = explode(" ",$preeval);
				if($eval[1]!=0){ $hmmeval[$hmmi] = $eval[1]."\n"; }
				else{ $hmmeval[$hmmi] = "5e-310\n"; }
				$hmmgroup[$hmmi] = $eval[9]."\n";
				
				// $preeval = preg_replace ('#\s+#' , ' ' , $log[$i+7]);
				// $eval = explode(" ",$preeval);
				// if($eval[1]!=0){ $hmmeval1[$hmmi] = $eval[1]."\n"; }
				// else{ $hmmeval1[$hmmi] = "5e-310\n"; }
				// $hmmgroup1[$hmmi] = $eval[9]."\n";
			}
			$hmmi++;
		}
	}
	$correct=0;
	$second=0;
	$incorrect=0;
	$buffer="";
	$chars = array(";", "/");
	$fixedgrp = str_replace($chars, "", $seqgrp["name"][$grpcount[(($ia+1)*2)-1]]);
	$rgrp = trim($fixedgrp);
	for($ix=0;$ix<count($hmmname);$ix++){
		if(strpos(trim($hmmgroup[$ix]), $rgrp)!==FALSE or strpos($rgrp, trim($hmmgroup[$ix]))!==FALSE){
			echo "Sequence was in group: ".$rgrp." and was classified to group ".$hmmgroup[$ix];
			echo "Sequence ".$ix." is correct!\n";
			$buffer .= "Sequence was in group: ".$rgrp." and was classified to group ".$hmmgroup[$ix];
			$buffer .= "Sequence ".$ix." is correct!\n";
			$buffer .= $hmmeval[$ix];
			$correct++;
		} else {
			echo "Sequence was in group: ".$rgrp." and was classified to group ".$hmmgroup[$ix];
			echo "Sequence ".$ix." is incorrect!\n";
			$buffer .= "Sequence was in group: ".$rgrp." and was classified to group ".$hmmgroup[$ix];
			$buffer .= "Sequence ".$ix." is incorrect!\n";
			$buffer .= $hmmeval[$ix];
			$incorrect++;
		}
	}
	echo $correct." of ".($incorrect+$correct)." sequences were classified correctly!\n";
	$buffer .= $correct." of ".($incorrect+$correct)." sequences were classified correctly!\n";
	$puffer .= $buffer;
	$ocorrect = $ocorrect + $correct;
	$oincorrect = $oincorrect + $incorrect;
}
echo "overall score: ".$ocorrect." of ".($oincorrect+$ocorrect)."\n";
$puffer .= "overall score: ".$ocorrect." of ".($oincorrect+$ocorrect)."\n";
$outputfile = "./statistics/completeoutput-".$argv[2]."-".$argv[3].".temp";
if (!$handle = fopen($outputfile, "w")){
	echo "cannot open file ".$outputfile."\n";
	exit;
} else{
	if(!fwrite($handle, $puffer)){
		echo "cannot write to file\n";
		exit;
	} else { echo "written new outputfile\n"; }
}
system("rm -R ".$ddir."*");
$of=$ocorrect.";".$oincorrect.";".($oincorrect+$ocorrect).";".$argv[2]."\n";
$outputfile = "./statistics/complete-".$argv[2]."-".$argv[3].".txt";
if (!$handle = fopen($outputfile, "w")){
	echo "cannot open file ".$outputfile."\n";
	exit;
} else{
	if(!fwrite($handle, $of)){
		echo "cannot write to file\n";
		exit;
	} else { echo "written new outputfile\n"; }
}
fclose($handle);

echo "done for ".$argv[2]." percent.\n";
?>
